package com.tngtech.confluence.plugins.tableenhancer

import com.tngtech.confluence.plugins.tableenhancer.ConfluenceTableService.CellColor
import com.tngtech.confluence.plugins.tableenhancer.Options.DeviceType
import com.tngtech.confluence.plugins.tableenhancer.Options.RowNumbers
import com.tngtech.confluence.plugins.tableenhancer.Options.RowNumbersBgColor
import com.tngtech.confluence.plugins.tableenhancer.Options.Sort
import com.tngtech.confluence.plugins.tableenhancer.Options.Sort.Order
import com.tngtech.confluence.plugins.tableenhancer.Options.TotalLine
import com.tngtech.confluence.plugins.tableenhancer.Options.TotalLine.DecimalMark
import com.tngtech.confluence.plugins.tableenhancer.test.BaseSpec
import com.tngtech.confluence.plugins.tableenhancer.test.Tables
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element
import org.jsoup.parser.Tag
import org.jsoup.select.Elements

class RowNumberServiceTest extends BaseSpec {

    private RowNumberService underTest

    private ConfluenceTableService confluenceTableService = Mock(){
        newTd(_) >> { CellColor color ->
            return new Element(Tag.valueOf('td'), '')
        }
        clone(_) >> { Element elem ->
            Element result = elem.clone()
            result.removeAttr('id')
            return result
        }
    }

    def setup() {
        underTest = new RowNumberService(confluenceTableService)
    }

    def "'addRowNumbersTo' should not change anything if #options"() {

        given:
        def table = Tables.SIMPLE.firstTable

        when:
        underTest.addRowNumbersTo(table, options)

        then:
        table == Tables.SIMPLE.firstTable
        0 * _._

        where:
        options << [
                optionsFor([ deviceType: DeviceType.DESKTOP ]),
                optionsFor([ deviceType: DeviceType.MOBILE ]),

                optionsFor([ numberOfFixedRows: 0 ]),
                optionsFor([ numberOfFixedColumns: 0 ]),

                optionsFor([ sort: new Sort(0, Order.ASCENDING) ]),
                optionsFor([ sort: new Sort(1, Order.DESCENDING) ]),

                optionsFor([ totalLine: new TotalLine(true, DecimalMark.POINT) ]),
                optionsFor([ totalLine: new TotalLine(true, DecimalMark.COMMA) ]),
        ]
    }

    def "'addRowNumbersTo' should not throw 'IllegalArgumentException' if element contains no table for 'NONE'"() {

        given:
        def divElement = (Tables.NONE.content as Document).body().child(0)
        def options = optionsFor([:])

        when:
        underTest.addRowNumbersTo(divElement, options)

        then:
        noExceptionThrown()
    }

    def "'addRowNumbersTo' should throw 'IllegalArgumentException' if element contains no table for 'options'"() {

        given:
        def bodyElement = (Tables.NONE.content as Document).body()
        def options = optionsFor([ rowNumbers: RowNumbers.ONCE_BEFORE_SORTING ])

        when:
        underTest.addRowNumbersTo(bodyElement, options)

        then:
        thrown IllegalArgumentException
    }

    def "'addRowNumbersTo' should add column with consecutive numbers if 'ONCE_BEFORE_SORTING'"() {

        given:
        def table = Tables.SIMPLE.firstTable
        def options = optionsFor([ rowNumbers: RowNumbers.ONCE_BEFORE_SORTING ])

        when:
        underTest.addRowNumbersTo(table, options)

        then:
        table.$('tr > *:first-child') == [ elemConfThEmpty(), *elemTdsWith('1', '2', '3', '4') ]

        // TODO how to :(
//        1 * confluenceTableService.clone(_) >> { Element elem -> elem }
//        4 * confluenceTableService.newTd(_ as CellColor)
//        0 * _._
    }

    def "'addRowNumbersTo' should add column and attribute 'data-sorter=false' on header if 'INDEPENDENT_FROM_SORTING'()"() {

        given:
        def table = Tables.SIMPLE.firstTable
        def options = optionsFor([ rowNumbers: RowNumbers.INDEPENDENT_FROM_SORTING ])

        when:
        underTest.addRowNumbersTo(table, options)

        then:
        table.$('tr > *:first-child') == [ elemConfThEmpty([ 'data-sorter': 'false' ]), *elemTdsWith('1', '2', '3', '4') ]

//        1 * confluenceTableService.clone(_ as Element)
//        4 * confluenceTableService.newTd(_ as CellColor)
//        0 * _._
    }

    def "'addRowNumbersTo' should use grey columns for if no background color is set"() {

        given:
        def table = Tables.COLORED.firstTable
        def options = optionsFor([ rowNumbers: RowNumbers.ONCE_AFTER_SORTING ])

        when:
        underTest.addRowNumbersTo(table, options)

        then:
        table.$('tr > *:first-child') == [ elemConfThBlueEmpty(), elemTdWith('1') ]

//        1 * confluenceTableService.clone(_ as Element)
//        1 * confluenceTableService.newTd(CellColor.GREY)
//        0 * _._
    }

    def "'addRowNumbersTo' should use transparent row number columns if 'TRANSPARENT' background color is set"() {

        given:
        def table = Tables.COLORED.firstTable
        def options = optionsFor([
                rowNumbers       : RowNumbers.ONCE_AFTER_SORTING,
                rowNumbersBgColor: RowNumbersBgColor.TRANSPARENT
        ])

        when:
        underTest.addRowNumbersTo(table, options)

        then:
        table.$('tr > *:first-child') == [ elemConfThBlueEmpty(), elemTdWith('1') ]

//        1 * confluenceTableService.clone(_ as Element)
//        1 * confluenceTableService.newTd(CellColor.NONE)
//        0 * _._
    }

    def "'addRowNumbersTo' should use red row number columns if 'RED' background color is set"() {

        given:
        def table = Tables.COLORED.firstTable
        def options = optionsFor([
                rowNumbers       : RowNumbers.ONCE_AFTER_SORTING,
                rowNumbersBgColor: RowNumbersBgColor.RED
        ])

        when:
        underTest.addRowNumbersTo(table, options)

        then:
        table.$('tr > *:first-child') == [ elemConfThBlueEmpty(), elemTdWith('1') ]

//        1 * confluenceTableService.clone(_ as Element)
//        1 * confluenceTableService.newTd(CellColor.RED)
//        0 * _._
    }

    def "'addRowNumbersTo' should use same attributes besides 'id' if 'AS_RIGHT_NEXT_CELL' background color is set"() {

        given:
        def table = Tables.COLORED.firstTable
        def options = optionsFor([
                rowNumbers       : RowNumbers.ONCE_AFTER_SORTING,
                rowNumbersBgColor: RowNumbersBgColor.AS_RIGHT_NEXT_CELL,
        ])

        when:
        underTest.addRowNumbersTo(table, options)

        then:
        table.$('tr > *:first-child') == [ elemConfThBlueEmpty(), elemConfTdGreenWith('1') ]

//        2 * confluenceTableService.clone(_ as Element)
//        0 * _._
    }

    def "'addRowNumbersTo' should just add row numbers to top level table but not inner tables"() {

        given:
        Options options = optionsFor([ rowNumbers: RowNumbers.ONCE_BEFORE_SORTING ])
        Element table = Tables.WITH_INNER_TABLES.firstTable

        when:
        underTest.addRowNumbersTo(table, options)

        then:
        table.children().$('> tr > td:first-child').size() == 4

        table.children().$('> tr:nth-child(1) > th:first-child') == [ elemConfThEmpty() ]
        table.children().$('> tr > td:first-child') == elemTdsWith('1', '2', '3', '4')

        // => check inner tables that they do have neither sorting nor row number enhancement
        Elements innerTables = table.$('td > table')
        innerTables.$('th').size() == 4 * 2
        innerTables.$('td').size() == 4 * 2

//        1 * confluenceTableService.clone(_ as Element)
//        4 * confluenceTableService.newTd(CellColor.GREY)
//        0 * _._
    }

    def "'addRowNumbersTo' should just add row numbers even on table without heading"() {

        given:
        def table = Tables.WITHOUT_HEADING.firstTable
        def options = optionsFor([ rowNumbers: RowNumbers.INDEPENDENT_FROM_SORTING ])

        when:
        underTest.addRowNumbersTo(table, options)

        then:
        table.$('> tbody > tr > td:first-child').size() == 3

        table.$('> tbody > tr > td:first-child') == elemTdsWith('1', '2', '3')

//        3 * confluenceTableService.newTd(CellColor.GREY)
//        0 * _._
    }

    def "'adjustRowNumbers' should not change anything if #options"() {

        given:
        def table = Tables.SIMPLE.firstTable
        underTest.addRowNumbersTo(table, options)

        when:
        underTest.adjustRowNumbers(table, options)

        then:
        table.toString() == Tables.SIMPLE.firstTable.toString()
        0 * _._

        where:
        options << [
                optionsFor([ deviceType: DeviceType.DESKTOP ]),
                optionsFor([ deviceType: DeviceType.MOBILE ]),

                optionsFor([ sort: new Sort(0, Order.ASCENDING) ]),
                optionsFor([ sort: new Sort(1, Order.DESCENDING) ]),

                optionsFor([ totalLine: new TotalLine(true, DecimalMark.POINT) ]),
                optionsFor([ totalLine: new TotalLine(true, DecimalMark.COMMA) ]),
        ]
    }

    def "'adjustRowNumbers' should not throw 'IllegalArgumentException' if element contains no table for 'NONE'"() {

        given:
        def divElement = (Tables.NONE.content as Document).body().child(0)
        def options = optionsFor([:])

        when:
        underTest.adjustRowNumbers(divElement, options)

        then:
        noExceptionThrown()
    }

    def "'adjustRowNumbers' should not throw 'IllegalArgumentException' if element contains no table for 'ONCE_BEFORE_SORTING'"() {

        given:
        def bodyElement = (Tables.NONE.content as Document).body()
        def options = optionsFor([ rowNumbers: RowNumbers.ONCE_BEFORE_SORTING ])

        when:
        underTest.adjustRowNumbers(bodyElement, options)

        then:
        noExceptionThrown()
    }

    def "'adjustRowNumbers' should throw 'IllegalArgumentException' if element contains no table"() {

        given:
        def bodyElement = (Tables.NONE.content as Document).body()
        def options = optionsFor([ rowNumbers: RowNumbers.ONCE_AFTER_SORTING ])

        when:
        underTest.adjustRowNumbers(bodyElement, options)

        then:
        thrown IllegalArgumentException
    }

    def "'adjustRowNumbers' should throw 'IllegalStateException' if given table has no row numbers"() {

        given:
        def bodyElement = Tables.SIMPLE.firstTable
        def options = optionsFor([ rowNumbers: RowNumbers.INDEPENDENT_FROM_SORTING ])

        when:
        underTest.adjustRowNumbers(bodyElement, options)

        then:
        thrown IllegalStateException
    }

    def "'adjustRowNumbers' should not adjust row numbers if 'ONCE_BEFORE_SORTING'"() {

        given:
        def table = Tables.SIMPLE_UNSORTED_ROWNUMS.firstTable
        def options = optionsFor([ rowNumbers: RowNumbers.ONCE_BEFORE_SORTING ])

        when:
        underTest.adjustRowNumbers(table, options)

        then:
        table.select('tr > *:first-child') == [ elemConfThEmpty(), *elemConfTdsGreyWith('4', '1', '3', '2') ]
        0 * _._
    }

    def "'adjustRowNumbers' should adjust row numbers if 'INDEPENDENT_FROM_SORTING'"() {

        given:
        def table = Tables.SIMPLE_UNSORTED_ROWNUMS.firstTable
        def options = optionsFor([ rowNumbers: RowNumbers.INDEPENDENT_FROM_SORTING ])

        when:
        underTest.adjustRowNumbers(table, options)

        then:
        table.select('tr > *:first-child') == [ elemConfThEmpty(), *elemConfTdsGreyWith('1', '2', '3', '4') ]
        0 * _._
    }
}
