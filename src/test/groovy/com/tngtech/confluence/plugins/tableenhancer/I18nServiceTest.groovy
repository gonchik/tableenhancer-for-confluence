package com.tngtech.confluence.plugins.tableenhancer

import com.atlassian.confluence.util.i18n.I18NBean
import com.atlassian.confluence.util.i18n.I18NBeanFactory
import com.tngtech.confluence.plugins.tableenhancer.test.BaseSpec


class I18nServiceTest extends BaseSpec {

    private I18nService underTest

    private I18NBean i18nBean = Mock(){
        getText(_, _) >> { String key, Object[] args -> key }
    }
    private I18NBeanFactory i18nBeanFactory = Mock(){
        getI18NBean() >> { i18nBean }
    }

    def setup() {
        underTest = new I18nService(i18nBeanFactory)
    }

    def "'noTableFoundWarning' should return correct value"() {

        expect:
        underTest.noTableFoundWarning() == I18nService.KEY_I18N_WARNING_NO_TABLE_FOUND
    }

    def "'firstRowMustBeHeadingToSortTableWarning' should return correct value"() {

        expect:
        underTest.firstRowMustBeHeadingToSortTableWarning() == I18nService.KEY_I18N_WARNING_FIRST_ROW_MUST_BE_HEADING_TO_SORT_TABLE
    }

    def "'sortColumnNotParsableWarning' should call 'getText' with correct paramters and return correct result"() {

        given:
        String sortColumn = 'sortBy 1'

        when:
        def result = underTest.sortColumnNotParsableWarning(sortColumn)

        then:
        result == I18nService.KEY_I18N_WARNING_SORT_COLUMN_INDEX_NOT_PARSEABLE

        1 * i18nBean.getText(I18nService.KEY_I18N_WARNING_SORT_COLUMN_INDEX_NOT_PARSEABLE, [ sortColumn ]) >> { key, args -> key }
        0 * _
    }

    def "'sortColumnOutOfBoundWarning' should call 'getText' with correct paramters"() {

        given:
        int sortColumn = 7
        int size = 4

        when:
        def result = underTest.sortColumnOutOfBoundWarning(sortColumn, size)

        then:
        result == I18nService.KEY_I18N_WARNING_SORT_COLUMN_INDEX_OUT_OF_BOUND

        1 * i18nBean.getText(I18nService.KEY_I18N_WARNING_SORT_COLUMN_INDEX_OUT_OF_BOUND, [ size, sortColumn ]) >> { key, args -> key }
        0 * _
    }
}
