package com.tngtech.confluence.plugins.tableenhancer

import com.tngtech.confluence.plugins.tableenhancer.Options.DeviceType
import com.tngtech.confluence.plugins.tableenhancer.Options.RowNumbers
import com.tngtech.confluence.plugins.tableenhancer.Options.Sort
import com.tngtech.confluence.plugins.tableenhancer.Options.Sort.Order
import com.tngtech.confluence.plugins.tableenhancer.Options.TotalLine
import com.tngtech.confluence.plugins.tableenhancer.Options.TotalLine.DecimalMark
import com.tngtech.confluence.plugins.tableenhancer.test.BaseSpec
import com.tngtech.confluence.plugins.tableenhancer.test.Tables
import org.jsoup.nodes.Element
import spock.lang.Unroll

class FixRowColServiceTest extends BaseSpec {

    private FixRowColService underTest

    def setup() {
        underTest = new FixRowColService()
    }

    @Unroll
    def "'wrapWithFixTag' should not change anything if #options"() {

        given:
        Element table = Tables.SIMPLE.firstTable

        when:
        underTest.wrapWithFixTag(table, options)

        then:
        table == Tables.SIMPLE.firstTable

        where:
        options << [
                optionsFor([ deviceType: DeviceType.DESKTOP ]),
                optionsFor([ deviceType: DeviceType.MOBILE ]),

                optionsFor([ numberOfFixedRows: -1 ]),
                optionsFor([ numberOfFixedRows: 0 ]),
                optionsFor([ numberOfFixedColumns: -1 ]),
                optionsFor([ numberOfFixedColumns: 0 ]),

                optionsFor([ rowNumbers: RowNumbers.ONCE_BEFORE_SORTING ]),
                optionsFor([ rowNumbers: RowNumbers.INDEPENDENT_FROM_SORTING ]),

                optionsFor([ sort: new Sort(0, Order.ASCENDING) ]),
                optionsFor([ sort: new Sort(1, Order.DESCENDING) ]),

                optionsFor([ totalLine: new TotalLine(true, DecimalMark.POINT) ]),
                optionsFor([ totalLine: new TotalLine(true, DecimalMark.COMMA) ]),
        ]
    }

    @Unroll
    def "'wrapWithFixTag' should wrap table with fixed rows #fixedRows and cols #fixedCols if #options"() {

        given:
        Element table = Tables.SIMPLE.firstTable
        Options options = optionsFor([ numberOfFixedRows: fixedRows, numberOfFixedColumns: fixedCols ])

        when:
        underTest.wrapWithFixTag(table, options)

        then:
        def tableParent = table.parent()
        tableParent.tagName() == 'div'
        tableParent.hasClass('fixed-headers-container')
        tableParent.attr('data-fixed-rows') == "${fixedRows}"
        tableParent.attr('data-fixed-columns') == "${fixedCols}"

        where:
        fixedRows | fixedCols
        1         | 0
        2         | 0
        0         | 1
        0         | 2
        1         | 1
        2         | 1
        1         | 2
        2         | 2
    }
}
