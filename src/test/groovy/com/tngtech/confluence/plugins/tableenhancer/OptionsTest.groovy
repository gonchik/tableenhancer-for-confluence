package com.tngtech.confluence.plugins.tableenhancer

import com.atlassian.util.concurrent.ThreadFactories.Default;
import com.tngtech.confluence.plugins.tableenhancer.ConfluenceTableService.CellColor
import com.tngtech.confluence.plugins.tableenhancer.Options.DeviceType
import com.tngtech.confluence.plugins.tableenhancer.Options.RowNumbers
import com.tngtech.confluence.plugins.tableenhancer.Options.RowNumbersBgColor
import com.tngtech.confluence.plugins.tableenhancer.Options.Sort
import com.tngtech.confluence.plugins.tableenhancer.Options.Sort.Order
import com.tngtech.confluence.plugins.tableenhancer.Options.TotalLine
import com.tngtech.confluence.plugins.tableenhancer.Options.TotalLine.DecimalMark
import com.tngtech.confluence.plugins.tableenhancer.test.BaseSpec

class OptionsTest extends BaseSpec {

    def "'new Options(...)' should throw 'NullPointerException' if 'deviceType' is 'null'"() {

        given:
        def rowNumbers = RowNumbers.NONE
        def rowNumbersBgColor = RowNumbersBgColor.GREY
        def sort = Sort.NONE
		def numberOfFixedRows = 0
		def numberOfFixedColumns = 0
        def warningMessages = []

        when:
        new Options(null, rowNumbers, rowNumbersBgColor, sort, TotalLine.NONE, numberOfFixedRows, numberOfFixedColumns, warningMessages)

        then:
        thrown NullPointerException
    }

    def "'new Options(...)' should throw 'NullPointerException' if 'rowNumbers' is 'null'"() {

        given:
        def deviceType = DeviceType.DESKTOP
        def rowNumbersBgColor = RowNumbersBgColor.GREY
        def sort = Sort.NONE
        def numberOfFixedRows = 0
		def numberOfFixedColumns = 0
        def warningMessages = []

        when:
        new Options(deviceType, null, rowNumbersBgColor, sort, TotalLine.NONE, numberOfFixedRows, numberOfFixedColumns, warningMessages)

        then:
        thrown NullPointerException
    }

    def "'new Options(...)' should throw 'NullPointerException' if 'rowNumbersBgColor' is 'null'"() {

        given:
        def deviceType = DeviceType.DESKTOP
        def rowNumbers = RowNumbers.NONE
        def sort = Sort.NONE
        def numberOfFixedRows = 0
		def numberOfFixedColumns = 0
        def warningMessages = []

        when:
        new Options(deviceType, rowNumbers, null, sort, TotalLine.NONE, numberOfFixedRows, numberOfFixedColumns, warningMessages)

        then:
        thrown NullPointerException
    }

    def "'new Options(...)' should throw 'NullPointerException' if 'sort' is 'null'"() {

        given:
        def deviceType = DeviceType.DESKTOP
        def rowNumbers = RowNumbers.NONE
        def rowNumbersBgColor = RowNumbersBgColor.GREY
        def numberOfFixedRows = 0
		def numberOfFixedColumns = 0
        def warningMessages = []

        when:
        new Options(deviceType, rowNumbers, rowNumbersBgColor, null, TotalLine.NONE, numberOfFixedRows, numberOfFixedColumns, warningMessages)

        then:
        thrown NullPointerException

    }

    def "'new Options(...)' should throw 'NullPointerException' if 'warningMessages' is 'null'"() {

        given:
        def deviceType = DeviceType.DESKTOP
        def rowNumbers = RowNumbers.NONE
        def rowNumbersBgColor = RowNumbersBgColor.GREY
        def sort = Sort.NONE
		def numberOfFixedRows = 0
		def numberOfFixedColumns = 0

        when:
        new Options(deviceType, rowNumbers, rowNumbersBgColor, sort, TotalLine.NONE, numberOfFixedRows, numberOfFixedColumns, null)

        then:
        thrown NullPointerException
    }

    def "'new Sort(...)' should throw 'IllegalArgumentException' if 'sortOrder' is 'null'"() {

        given:

        when:
        new Sort(1, null)

        then:
        thrown IllegalArgumentException
    }

    def "'isMobile' should return 'false' for 'DESKTOP'"() {

        given:
        Options underTest = optionsFor([ deviceType: DeviceType.DESKTOP ])

        expect:
        !underTest.isMobile()
    }

    def "'isMobile' should return 'true' for 'MOBILE'"() {

        given:
        Options underTest = optionsFor([ deviceType: DeviceType.MOBILE ])

        expect:
        underTest.isMobile()
    }

    def "'getResourcesContext' should return correct desktop context for 'DESKTOP'"() {

        given:
        Options underTest = optionsFor([ deviceType: DeviceType.DESKTOP ])

        expect:
        underTest.resourcesContext == 'com.tngtech.confluence.plugins.tableenhancer'
    }

    def "'getResourcesContext' should return correct mobile context for 'MOBILE'"() {

        given:
        Options underTest = optionsFor([ deviceType: DeviceType.MOBILE ])

        expect:
        underTest.resourcesContext == 'com.tngtech.confluence.plugins.tableenhancer.mobile'
    }

    def "'hasRowNumbers' should return 'false' for 'NONE'"() {

        given:
        Options underTest = optionsFor([ rowNumbers: RowNumbers.NONE ])

        expect:
        !underTest.hasRowNumbers()
    }

    def "'hasRowNumbers' should return 'true' for 'INDEPENDENT_FROM_SORTING'"() {

        given:
        Options underTest = optionsFor([ rowNumbers: RowNumbers.INDEPENDENT_FROM_SORTING ])

        expect:
        underTest.hasRowNumbers()
    }

    def "'getRowNumbersTableClass' should return 'EmptyString' for 'NONE'"() {

        given:
        Options underTest = optionsFor([ rowNumbers: RowNumbers.NONE ])

        expect:
        underTest.rowNumbersTableClass.isEmpty()
    }

    def "'getRowNumbersTableClass' should return correct string for 'ONCE_BEFORE_SORTING'"() {

        given:
        Options underTest = optionsFor([ rowNumbers: RowNumbers.ONCE_BEFORE_SORTING ])

        expect:
        underTest.rowNumbersTableClass == 'tableenhancer-rownums-onceBeforeSorting'
    }

    def "'rowNumbersColumnIsSortable' should return 'false' for 'INDEPENDENT_FROM_SORTING'"() {

        given:
        Options underTest = optionsFor([ rowNumbers: RowNumbers.INDEPENDENT_FROM_SORTING ])

        expect:
        !underTest.rowNumbersColumnIsSortable()
    }

    def "'rowNumbersColumnIsSortable' should return 'true' for 'ONCE_BEFORE_SORTING'"() {

        given:
        Options underTest = optionsFor([ rowNumbers: RowNumbers.ONCE_BEFORE_SORTING ])

        expect:
        underTest.rowNumbersColumnIsSortable()
    }

    def "'rowNumbersMustBeAdjustedAfterSorting' should return 'false' for 'ONCE_BEFORE_SORTING'"() {

        given:
        Options underTest = optionsFor([ rowNumbers: RowNumbers.ONCE_BEFORE_SORTING ])

        expect:
        !underTest.rowNumbersMustBeAdjustedAfterSorting()
    }

    def "'rowNumbersMustBeAdjustedAfterSorting' should return 'true' for 'INDEPENDENT_FROM_SORTING'"() {

        given:
        Options underTest = optionsFor([ rowNumbers: RowNumbers.INDEPENDENT_FROM_SORTING ])

        expect:
        underTest.rowNumbersMustBeAdjustedAfterSorting()
    }

    def "'rowNumbersMustBeAdjustedAfterSorting' should return 'true' for 'ONCE_AFTER_SORTING'"() {

        given:
        Options underTest = optionsFor([ rowNumbers: RowNumbers.ONCE_AFTER_SORTING ])

        expect:
        underTest.rowNumbersMustBeAdjustedAfterSorting()
    }

    def "'getRowNumbersBgColor' should return 'NONE' for 'TRANSPARENT'"() {

        given:
        Options underTest = optionsFor([ rowNumbersBgColor: RowNumbersBgColor.TRANSPARENT ])

        expect:
        underTest.rowNumbersBgColor == CellColor.NONE
    }

    def "'getRowNumbersBgColor' should return 'NONE' for 'GREY'"() {

        given:
        Options underTest = optionsFor([ rowNumbersBgColor: RowNumbersBgColor.GREY ])

        expect:
        underTest.getRowNumbersBgColor() == CellColor.GREY
    }

    def "'getRowNumbersBgColor' should return 'NONE' for 'AsRightNextCell'"() {

        given:
        Options underTest = optionsFor([ rowNumbersBgColor: RowNumbersBgColor.AS_RIGHT_NEXT_CELL ])

        expect:
        underTest.getRowNumbersBgColor() == null
    }

    def "'isRowNumberBgColorAsRightNextCell' should return 'false' for 'TRANSPARENT'"() {

        given:
        Options underTest = optionsFor([ rowNumbersBgColor: RowNumbersBgColor.TRANSPARENT ])

        expect:
        !underTest.rowNumberBgColorAsRightNextCell
    }

    def "'isRowNumberBgColorAsRightNextCell' should return 'true' for 'AS_RIGHT_NEXT_CELL'"() {

        given:
        Options underTest = optionsFor([ rowNumbersBgColor: RowNumbersBgColor.AS_RIGHT_NEXT_CELL ])

        expect:
        underTest.rowNumberBgColorAsRightNextCell
    }

    def "'sortTable' should return 'false' for 'NONE'"() {

        given:
        Options underTest = optionsFor([ sort: Sort.NONE ])

        expect:
        !underTest.sortTable()
    }

    def "'sortTable' should return 'true' for column '0' and 'ASCENDING'"() {

        given:
        Options underTest = optionsFor([ sort: new Sort(0, Order.ASCENDING) ])

        expect:
        underTest.sortTable()
    }

    def "'sortColumnIndex' should throw 'IllegalStateException' for 'NONE'"() {

        given:
        Options underTest = optionsFor([ sort: Sort.NONE ])

        when:
        underTest.sortColumnIndex()

        then:
        thrown IllegalStateException
    }

    def "'sortColumnIndex' should return actual column index"() {

        given:
        Options underTest = optionsFor([ sort: new Sort(2, Order.DESCENDING) ])

        expect:
        underTest.sortColumnIndex() == 2
    }

    def "'sortColumnIndex' should return incremented by one 'columnIndex' if 'hasRowNumbers' as well"() {

        given:
        Options underTest = optionsFor([
                rowNumbers: RowNumbers.ONCE_BEFORE_SORTING,
                sort      : new Sort(3, Order.ASCENDING)
        ])

        expect:
        underTest.sortColumnIndex() == 4
    }

    def "sortColumnUserIndex should throw 'IllegalStateException' for 'NONE'"() {

        given:
        Options underTest = optionsFor([ sort: Sort.NONE ])

        when:
        underTest.sortColumnUserIndex()

        then:
        thrown IllegalStateException
    }

    def "'sortColumnUserIndex' should return 'columnIndex' incremented by one'"() {

        given:
        Options underTest = optionsFor([ sort: new Sort(5, Order.DESCENDING) ])

        expect:
        underTest.sortColumnUserIndex() == 6
    }

    def "'sortColumnUserIndex' should return 'columnIndex' incremented further by one if table should 'hasRowNumbers'"() {

        given:
        Options underTest = optionsFor([
                rowNumbers: RowNumbers.ONCE_BEFORE_SORTING,
                sort      : new Sort(6, Order.ASCENDING)
        ])

        expect:
        underTest.sortColumnUserIndex() == 7
    }

    def "'isSortColumnIndexValidFor' should throw 'IllegalStateException' for 'NONE'"() {

        given:
        Options underTest = optionsFor([ sort: Sort.NONE ])

        when:
        underTest.isSortColumnIndexValidFor(0)

        then:
        thrown IllegalStateException
    }

    def "'isSortColumnIndexValidFor' should return 'false' for negative 'columnIndex'"() {

        given:
        Options underTest = optionsFor([ rowNumbers: RowNumbers.NONE, sort: new Sort(-1, Order.DESCENDING) ])

        expect:
        !underTest.isSortColumnIndexValidFor(10)
    }

    def "'isSortColumnIndexValidFor' should return 'false' for 'columnIndex' '0' if table has size '0''"() {

        given:
        Options underTest = optionsFor([
                rowNumbers: RowNumbers.NONE,
                sort      : new Sort(0, Order.ASCENDING)
        ])

        expect:
        !underTest.isSortColumnIndexValidFor(0)
    }

    def "'isSortColumnIndexValidFor' should return 'true' for 'columnIndex' '0' if table has size '10'"() {
        given:
        Options underTest = optionsFor([ rowNumbers: RowNumbers.NONE, sort: new Sort(0, Order.ASCENDING) ])

        expect:
        underTest.isSortColumnIndexValidFor(10)
    }

    def "'isSortColumnIndexValidFor' should return 'true' for 'columnIndex' '8' if table has size '9'"() {
        given:
        Options underTest = optionsFor([ rowNumbers: RowNumbers.NONE, sort: new Sort(8, Order.ASCENDING) ])

        expect:
        underTest.isSortColumnIndexValidFor(9)
    }

    def "'isSortColumnIndexValidFor' should return 'false' for 'columnIndex' '7' if table has size '7'"() {
        given:
        Options underTest = optionsFor([ rowNumbers: RowNumbers.NONE, sort: new Sort(7, Order.DESCENDING) ])

        expect:
        !underTest.isSortColumnIndexValidFor(7)
    }

    def "'isSortColumnIndexValidFor' should return 'false' for 'columnIndex' '5' if table has size '6' but has 'rowNumbers'"() {
        given:
        Options underTest = optionsFor([
            rowNumbers: RowNumbers.ONCE_BEFORE_SORTING,
            sort:       new Sort(5, Order.DESCENDING),
        ])

        expect:
        !underTest.isSortColumnIndexValidFor(6)
    }

    def "'sortDescending' should throw 'IllegalStateException' for 'NONE'"() {

        given:
        Options underTest = optionsFor([ sort: Sort.NONE ])

        when:
        underTest.sortDescending()

        then:
        thrown IllegalStateException
    }

    def "'sortDescending' should return 'false' for 'ASCENDING'"() {

        given:
        Options underTest = optionsFor([ sort: new Sort(12, Order.ASCENDING) ])

        expect:
        !underTest.sortDescending()
    }

    def "'sortDescending' should return 'true' for 'DESCENDING'"() {

        given:
        Options underTest = optionsFor([ sort: new Sort(13, Order.DESCENDING) ])

        expect:
        underTest.sortDescending()
    }

    def "'getSortOrderThClass' should return '' for 'NONE'"() {

        given:
        Options underTest = optionsFor([ sort: Sort.NONE ])

        expect:
        String result = underTest.sortOrderThClass.isEmpty()
    }

    def "'getSortOrderThClass' should return correct string for 'ASCENDING'"() {

        given:
        Options underTest = optionsFor([ sort: new Sort(15, Order.ASCENDING) ])

        expect:
        underTest.getSortOrderThClass() == 'tableenhancer-sort-asc'
    }

    def "'hasTotalLine' should return 'false' for 'NONE'"() {

        given:
        Options underTest = optionsFor([ totalLine: TotalLine.NONE ])

        expect:
        !underTest.hasTotalLine()
    }

    def "'hasTotalLine' should return 'true' for totalLine is 'true'"() {

        given:
        Options underTest = optionsFor([ totalLine: new TotalLine(true, DecimalMark.POINT) ])

        expect:
        underTest.hasTotalLine()
    }

    def "'useCommaAsDecimalMark' should return 'false' for 'POINT'"() {

        given:
        Options underTest = optionsFor([ totalLine: new TotalLine(true, DecimalMark.POINT) ])

        expect:
        !underTest.useCommaAsDecimalMark()
    }

    def "'useCommaAsDecimalMark' should return 'true' for 'COMMA'"() {

        given:
        Options underTest = optionsFor([ totalLine: new TotalLine(true, DecimalMark.COMMA) ])

        expect:
        underTest.useCommaAsDecimalMark()
    }


    def "'getNumberOfFixedRows' should return '1' for '1'"() {

        given:
        Options underTest = optionsFor([ numberOfFixedRows: 1 ])

        expect:
        underTest.getNumberOfFixedRows() == 1
    }

    def "'getNumberOfFixedColumns' should return '3' for '3'"() {

        given:
        Options underTest = optionsFor([ numberOfFixedColumns: 3 ])

        expect:
        underTest.getNumberOfFixedColumns() == 3
    }

    def "'getWarningMessages' should return 'UnmodifiableList'"() {

        given:
        Options underTest = optionsFor([ warningMessages: 'Warning 1' ])

        List<String> warningMessages = underTest.getWarningMessages()

        when:
        warningMessages.add(new String())

        then:
        thrown UnsupportedOperationException
    }

    def "'getWarningMessages' should return '[]' for given '[]'"() {

        given:
        Options underTest = optionsFor([:])

        expect:
        underTest.warningMessages.isEmpty()
    }

    def "'getWarningMessages' should return single given element"() {

        given:
        String warning = 'Warning 2'

        Options underTest = optionsFor([ warnings: [ warning ] ])

        expect:
        underTest.warningMessages == [ warning ]
    }

    def "'getWarningMessages' should return all given elements"() {

        given:
        String warning1 = 'Warning 3'
        String warning2 = 'Warning 4'
        String warning3 = 'Warning 5'

        Options underTest = optionsFor([ warnings: [ warning1, warning2, warning3 ] ])

        expect:
        underTest.warningMessages == [ warning1, warning2, warning3 ]
    }

    def "'equals' should return 'true' for equal 'Options'"() {

        given:
        Options o1 = optionsFor([
                deviceType: DeviceType.DESKTOP,
                rowNumbers: RowNumbers.ONCE_AFTER_SORTING,
                totalLine : new TotalLine(true, DecimalMark.COMMA),
        ])
        Options o2 = optionsFor([
                deviceType: DeviceType.DESKTOP,
                rowNumbers: RowNumbers.ONCE_AFTER_SORTING,
                totalLine : new TotalLine(true, DecimalMark.COMMA),
        ])

        expect:
        o1.equals(o2)
    }

    def "'equals' should return 'false' for unequal 'Options'"() {

        given:
        Options o1 = optionsFor([ deviceType: DeviceType.MOBILE, rowNumbers: RowNumbers.INDEPENDENT_FROM_SORTING ])
        Options o2 = optionsFor([ deviceType: DeviceType.MOBILE, rowNumbers: RowNumbers.ONCE_BEFORE_SORTING ])

        expect:
        !o1.equals(o2)
    }

    def "'equals' should return 'false' for unequal 'Options.sort'"() {

        given:
        Options o1 = optionsFor([ deviceType: DeviceType.MOBILE, sort: new Sort(0, Order.DESCENDING) ])
        Options o2 = optionsFor([ deviceType: DeviceType.MOBILE, sort: new Sort(1, Order.DESCENDING) ])

        expect:
        !o1.equals(o2)
    }

    def "'hashCode' should return same value for equal 'Options'"() {

        given:
        Options o1 = optionsFor([ rowNumbers: RowNumbers.ONCE_BEFORE_SORTING, sort: new Sort(2, Order.ASCENDING) ])
        Options o2 = optionsFor([ rowNumbers: RowNumbers.ONCE_BEFORE_SORTING, sort: new Sort(2, Order.ASCENDING) ])

        expect:
        o1.hashCode() == o2.hashCode()
    }
}
