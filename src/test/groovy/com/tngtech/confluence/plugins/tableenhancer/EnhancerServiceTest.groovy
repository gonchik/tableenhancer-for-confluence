package com.tngtech.confluence.plugins.tableenhancer

import com.atlassian.confluence.content.render.xhtml.ConversionContext
import com.atlassian.confluence.content.render.xhtml.XhtmlException
import com.atlassian.confluence.macro.MacroExecutionException
import com.atlassian.confluence.xhtml.api.MacroDefinition
import com.atlassian.confluence.xhtml.api.XhtmlContent
import com.atlassian.plugin.webresource.WebResourceManager
import com.tngtech.confluence.plugins.tableenhancer.Options.DeviceType
import com.tngtech.confluence.plugins.tableenhancer.Options.RowNumbers
import com.tngtech.confluence.plugins.tableenhancer.Options.Sort
import com.tngtech.confluence.plugins.tableenhancer.Options.Sort.Order
import com.tngtech.confluence.plugins.tableenhancer.Options.TotalLine
import com.tngtech.confluence.plugins.tableenhancer.Options.TotalLine.DecimalMark
import com.tngtech.confluence.plugins.tableenhancer.test.BaseSpec
import com.tngtech.confluence.plugins.tableenhancer.test.Tables
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element
import org.jsoup.select.Elements
import spock.lang.Unroll

class EnhancerServiceTest extends BaseSpec {

    private EnhancerService underTest

    private FixRowColService fixRowColService = Mock()
    private I18nService i18nService = Mock()
    private RowNumberService rowNumberService = Mock()
    private SorterService sorterService = Mock()
    private TotalLineService totalLineService = Mock()
    private WebResourceManager webResourceManager = Mock()
    private XhtmlContent xhtmlContent = Mock()

    private ConversionContext conversionContext = Mock()

    def setup() {
        underTest = new EnhancerService(fixRowColService, i18nService, rowNumberService, sorterService,
                totalLineService, webResourceManager, xhtmlContent)
    }

    def "'prepareWebResourcesFor' should add required web resources for desktop()"() {

        given:
        Options options = optionsFor([ deviceType: DeviceType.DESKTOP ])

        when:
        underTest.prepareWebResourcesFor(options)

        then:
        1 * webResourceManager.requireResourcesForContext(options.getResourcesContext())
        0 * _._
    }

    def "'prepareWebResourcesFor' should add required web resources for mobile()"() {

        given:
        Options options = optionsFor([ deviceType: DeviceType.MOBILE ])

        when:
        underTest.prepareWebResourcesFor(options)

        then:
        1 * webResourceManager.requireResourcesForContext(options.getResourcesContext())
        0 * _._
    }

    def "'createRenderedWarning' should throw 'MacroExecutionException' if 'xhtmlContent' throws any 'XhtmlException'"() {

        given:

        when:
        underTest.createRenderedWarning('Warning 1', conversionContext)

        then:
        thrown MacroExecutionException

        1 * xhtmlContent.convertMacroDefinitionToView(_ as MacroDefinition, conversionContext) >> {
            throw new XhtmlException()
        }
        0 * _._
    }

    def "'createRenderedWarning' should call 'xhtmlContent.convertMacroDefinitionToView'"() {

        given:
        String converted = '<div class="macro">...</div>'

        when:
        String result = underTest.createRenderedWarning('Warning 2', conversionContext)

        then:
        result == converted

        1 * xhtmlContent.convertMacroDefinitionToView(_ as MacroDefinition, conversionContext) >> converted
        0 * _._
    }

    @Unroll
    def "'enhanceTables' should add warning if body contains no table on #deviceType"() {

        given:
        Options options = optionsFor([ deviceType: deviceType ])
        String body = Tables.NONE.content

        when:
        String result = underTest.enhanceTables(options, body, conversionContext)

        then:
        (result as Document).$('.warning').size() == 1

        then: 1 * i18nService.noTableFoundWarning()
        then: 1 * xhtmlContent.convertMacroDefinitionToView(_ as MacroDefinition, conversionContext) >> {
            '<div class="warning">...</div>'
        }
        then: 0 * _._

        where:
        deviceType << [ DeviceType.DESKTOP, DeviceType.MOBILE ]
    }

    @Unroll
    def "'enhanceTables' should throw 'MacroExecutionException' if warning generation throws a 'XhtmlException' for #deviceType"() {

        given:
        Options options = optionsFor([ deviceType: deviceType ])
        String body = Tables.NONE.content


        when:
        underTest.enhanceTables(options, body, conversionContext)

        then:
        thrown MacroExecutionException

        then: 1 * i18nService.noTableFoundWarning()
        then: 1 * xhtmlContent.convertMacroDefinitionToView(_ as MacroDefinition, conversionContext) >> {
            throw new XhtmlException()
        }
        then: 0 * _._

        where:
        deviceType << [ DeviceType.DESKTOP, DeviceType.MOBILE ]
    }

    def "'enhanceTables' should add tableenhancer class and call all service methods even if no 'options' are specified on desktop"() {

        given:
        Options options = optionsFor([ deviceType: DeviceType.DESKTOP ])
        String body = Tables.SIMPLE.content

        when:
        String result = underTest.enhanceTables(options, body, conversionContext)

        then:
        def r = result as Document

        !r.$('.warning')
        r.$('table.confluenceTable.tableenhancer').size() == 1

//        Element table = r.$('table')[0] // TODO how to verify that table is correct?
        then: 1 * rowNumberService.addRowNumbersTo(_, options)
        then: 1 * sorterService.addSortClassTo(_, options)
        then: 1 * totalLineService.addTotalLineTo(_, options)
        then: 1 * fixRowColService.wrapWithFixTag(_, options)
        then: 0 * _._
    }

    def "'enhanceTables' should add tableenhancer class and call all service methods even if no 'options' are specified on mobile"() {

        given:
        Options options = optionsFor([ deviceType: DeviceType.MOBILE ])
        String body = Tables.SIMPLE.content

        when:
        String result = underTest.enhanceTables(options, body, conversionContext)

        then:
        def r = result as Document

        !r.$('.warning')
        r.$("table.confluenceTable.tableenhancer").size() == 1

        r.$('table.confluenceTable.tableenhancer').removeClass('tableenhancer')
        r == body as Document

        Element table = r.$('table')[0]
        then: 1 * rowNumberService.addRowNumbersTo(_, options)
        then: 1 * sorterService.addSortClassTo(_, options)
        then: 1 * sorterService.sort(_, options)
        then: 1 * rowNumberService.adjustRowNumbers(_, options)
        then: 1 * totalLineService.addTotalLineTo(_, options)
        then: 1 * fixRowColService.wrapWithFixTag(_, options)
        then: 0 * _._
    }

    def "'enhanceTables' should add warning if 'sorterService.addSortClassTo' throws 'MacroExecutionException' on desktop"() {

        given:
        Options options = optionsFor([ deviceType: DeviceType.DESKTOP ])
        String body = Tables.SIMPLE.content

        when:
        String result = underTest.enhanceTables(options, body, conversionContext)

        then:
        def r = result as Document

        r.$('.warning').size() == 1
        r.$('table.confluenceTable.tableenhancer').size() == 1

        Element table = r.$('table')[0]
        then: 1 * rowNumberService.addRowNumbersTo(_, options)
        then: 1 * sorterService.addSortClassTo(_, options) >> { throw new MacroExecutionException() }
        then: 1 * xhtmlContent.convertMacroDefinitionToView(_ as MacroDefinition, conversionContext) >> {
            '<div class="warning">...</div>'
        }
        then: 1 * totalLineService.addTotalLineTo(_, options)
        then: 1 * fixRowColService.wrapWithFixTag(_, options)
        then: 0 * _._
    }

    @Unroll
    def "'enhanceTables' should call all services methods for #tableDesc table"() {

        given:
        Options options = optionsFor([
                rowNumbers: RowNumbers.ONCE_BEFORE_SORTING,
                sort      : new Sort(0, Order.ASCENDING),
                totalLine : new TotalLine(true, DecimalMark.POINT),
        ])

        when:
        String result = underTest.enhanceTables(options, body, conversionContext)

        then:
        def r = result as Document

        !r.$('.warning')
        r.$('table.confluenceTable.tableenhancer').size() == 1

        Element table = r.$('table')[0]
        then: 1 * rowNumberService.addRowNumbersTo(_, options)
        then: 1 * sorterService.addSortClassTo(_, options)
        then: 1 * totalLineService.addTotalLineTo(_, options)
        then: 1 * fixRowColService.wrapWithFixTag(_, options)
        then: 0 * _._

        where:
        tableDesc                | body
        'simple not wrapped'     | Tables.SIMPLE_NOT_WRAPPED.content
        'simple further wrapped' | Tables.SIMPLE_FURTHER_WRAPPED.content
    }

    def "'enhanceTables' should call all service methods only for top level table even if it contains further tables on desktop"() {

        given:
        Options options = optionsFor([
                deviceType: DeviceType.DESKTOP,
                rowNumbers: RowNumbers.INDEPENDENT_FROM_SORTING,
                sort      : new Sort(0, Order.ASCENDING),
        ])
        String body = Tables.WITH_INNER_TABLES.content

        when:
        String result = underTest.enhanceTables(options, body, conversionContext)

        then:
        def r = result as Document

        !r.$('.warning')
        r.$('table.confluenceTable.tableenhancer').size() == 1

        Element table = r.$('body > div > table')[0]
        then: 1 * rowNumberService.addRowNumbersTo(_, options)
        then: 1 * sorterService.addSortClassTo(_, options)
        then: 1 * totalLineService.addTotalLineTo(_, options)
        then: 1 * fixRowColService.wrapWithFixTag(_, options)
        then: 0 * _._
    }

    def "'enhanceTables' should call all service methods only for top level table even if it contains further tables on mobile"() {

        given:
        Options options = optionsFor([
                deviceType: DeviceType.MOBILE,
                sort      : new Sort(0, Order.ASCENDING)
        ])
        String body = Tables.WITH_INNER_TABLES.content

        when:
        String result = underTest.enhanceTables(options, body, conversionContext)

        then:
        def r = result as Document

        !r.$('.warning')
        r.$('table.confluenceTable.tableenhancer').size() == 1

        Element table = r.$('body > div > table')[0]
        then: 1 * rowNumberService.addRowNumbersTo(_, options)
        then: 1 * sorterService.addSortClassTo(_, options)
        then: 1 * sorterService.sort(_, options)
        then: 1 * rowNumberService.adjustRowNumbers(_, options)
        then: 1 * totalLineService.addTotalLineTo(_, options)
        then: 1 * fixRowColService.wrapWithFixTag(_, options)
        then: 0 * _._
    }

    def "'enhanceTables' should call all service methods for every top level table on desktop"() {

        given:
        Options options = optionsFor([
                deviceType: DeviceType.DESKTOP,
                rowNumbers: RowNumbers.ONCE_AFTER_SORTING,
                sort      : new Sort(1, Order.DESCENDING),
        ])
        String body = Tables.MULTIPLE_TOP_LEVEL.content

        when:
        String result = underTest.enhanceTables(options, body, conversionContext)

        then:
        def r = result as Document

        !r.$('.warning')
        r.$('table.confluenceTable.tableenhancer').size() == 2

        Elements tables = r.$('table')
        tables.size() == 2

        then: 1 * rowNumberService.addRowNumbersTo(_, options)
        then: 1 * sorterService.addSortClassTo(_, options)
        then: 1 * totalLineService.addTotalLineTo(_, options)
        then: 1 * fixRowColService.wrapWithFixTag(_, options)
        then: 1 * rowNumberService.addRowNumbersTo(_, options)
        then: 1 * sorterService.addSortClassTo(_, options)
        then: 1 * totalLineService.addTotalLineTo(_, options)
        then: 1 * fixRowColService.wrapWithFixTag(_, options)
        then: 0 * _._
    }
}
