package com.tngtech.confluence.plugins.tableenhancer

import com.tngtech.confluence.plugins.tableenhancer.ConfluenceTableService.CellColor
import com.tngtech.confluence.plugins.tableenhancer.test.BaseSpec
import org.jsoup.nodes.Element

class ConfluenceTableServiceTest extends BaseSpec {

    private ConfluenceTableService underTest = new ConfluenceTableService()

    def "'newTd' should throw 'NullPointerException' for 'null'"() {

        given:

        when:
        underTest.newTd(null)

        then:
        thrown NullPointerException
    }

    def "'newTd' should return element having no color if 'cellColor' is none"() {

        given:

        when:
        Element result = underTest.newTd(CellColor.NONE)

        then:
        result == elemConfTdWith('')
    }

    def "'newTd' should return element having #expectedColor for 'cellColor' #cellColor"() {

        given:

        when:
        Element result = underTest.newTd(cellColor)

        then:
        result == "elemConfTd${expectedColor}With"('') // TODO really?
//        elemTdConfWith('', [ class: ' confluenceTd ' + cellColor.cssClass, 'data-highlight-colour': cellColor.dataHighlightColourValue ])


        where:
        cellColor         | expectedColor
        CellColor.GREY    | 'Grey'
        CellColor.RED     | 'Red'
        CellColor.GREEN   | 'Green'
        CellColor.BLUE    | 'Blue'
        CellColor.YELLOW  | 'Yellow'
    }

    def "'clone' should throw 'NullPointerException' for 'null'"() {

        given:

        when:
        underTest.clone(null)

        then:
        thrown NullPointerException
    }

    def "'clone' should return cloned element if element is simple"() {

        given:
        Element element = elemTdWith('test', [ class: 'test', attr1: 'val1' ])

        when:
        Element result = underTest.clone(element)

        then:
        !result.is(element)
        result == element
    }

    def "'clone' should return deeply cloned element if element is complex"() {

        given:
        Element element = elem('tr', '', [ class: 'row' ]).appendChild(elemTdWith('cell', [ class: 'test', attr1: 'val1' ]))

        when:
        Element result = underTest.clone(element)

        then:
        !result.is(element)
        result == element
    }
}
