package com.tngtech.confluence.plugins.tableenhancer

import com.atlassian.confluence.content.render.xhtml.ConversionContext
import com.atlassian.confluence.macro.MacroExecutionException
import com.tngtech.confluence.plugins.tableenhancer.Options.DeviceType
import com.tngtech.confluence.plugins.tableenhancer.Options.RowNumbers
import com.tngtech.confluence.plugins.tableenhancer.Options.Sort
import com.tngtech.confluence.plugins.tableenhancer.Options.Sort.Order
import com.tngtech.confluence.plugins.tableenhancer.Options.TotalLine
import com.tngtech.confluence.plugins.tableenhancer.Options.TotalLine.DecimalMark
import com.tngtech.confluence.plugins.tableenhancer.test.BaseSpec
import com.tngtech.confluence.plugins.tableenhancer.test.Tables
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element

class SorterServiceTest extends BaseSpec {

    private SorterService underTest

    private I18nService i18nService = Mock()

    private ConversionContext conversionContext = Mock()

    def setup() {
        underTest = new SorterService(i18nService)
    }

    def "'addSortClassTo' should not change anything if '#options'"() {

        given:
        def table = Tables.SIMPLE.firstTable

        when:
        underTest.addSortClassTo(table, options)

        then:
        table == Tables.SIMPLE.firstTable

        where:
        options << [
                optionsFor([ deviceType: DeviceType.DESKTOP ]),
                optionsFor([ deviceType: DeviceType.MOBILE ]),

                optionsFor([ numberOfFixedRows: 0 ]),
                optionsFor([ numberOfFixedColumns: 0 ]),

                optionsFor([ rowNumbers: RowNumbers.ONCE_BEFORE_SORTING ]),
                optionsFor([ rowNumbers: RowNumbers.INDEPENDENT_FROM_SORTING ]),

                optionsFor([ totalLine: new TotalLine(true, DecimalMark.POINT) ]),
                optionsFor([ totalLine: new TotalLine(true, DecimalMark.COMMA) ]),
        ]
    }

    def "'addSortClassTo' should not throw 'IllegalArgumentException' if body contains no table but \"empty\" options"() {

        given:
        def divElement = (Tables.NONE.content as Document).body().child(0)
        def options = optionsFor([:])

        when:
        underTest.addSortClassTo(divElement, options)

        then:
        noExceptionThrown()
    }

    def "'addSortClassTo' should throw 'IllegalArgumentException' if body contains no table"() {

        given:
        def divElement = (Tables.NONE.content as Document).body().child(0)
        def options = optionsFor([ sort: new Sort(0, Order.ASCENDING) ])

        when:
        underTest.addSortClassTo(divElement, options)

        then:
        thrown IllegalArgumentException
    }

    def "'addSortClassTo' should not thrown 'MacroExecutionException' if table has no header but \"empty\" options"() {

        given:
        Element table = Tables.WITHOUT_HEADING.firstTable
        Options options = optionsFor([:])

        when:
        underTest.addSortClassTo(table, options)

        then:
        table.toString() == Tables.WITHOUT_HEADING.firstTable.toString()
    }

    def "'addSortClassTo' should throw 'MacroExecutionException' if table has no header"() {

        given:
        def table = Tables.WITHOUT_HEADING.firstTable
        def options = optionsFor([ sort: new Sort(1, Order.ASCENDING) ])

        when:
        underTest.addSortClassTo(table, options)

        then:
        thrown MacroExecutionException
    }

    def "'addSortClassTo' should throw 'MacroExecutionException' if 'sortColumn' is negative"() {

        given:
        def table = Tables.SIMPLE.firstTable
        def options = optionsFor([ sort: new Sort(-1, Order.DESCENDING) ])

        when:
        underTest.addSortClassTo(table, options)

        then:
        thrown MacroExecutionException
    }

    def "'addSortClassTo' should throw 'MacroExecutionException' if 'sortColumn' is greater than column count"() {

        given:
        def table = Tables.SIMPLE.firstTable
        def options = optionsFor([ sort: new Sort(3, Order.ASCENDING) ])

        when:
        underTest.addSortClassTo(Tables.SIMPLE.firstTable, options)

        then:
        thrown MacroExecutionException
    }

    def "'addSortClassTo' should add 'sortOrderThClass' at column one if 'sortColumn' is '1'"() {

        given:
        def table = Tables.SIMPLE.firstTable
        def options = optionsFor([ sort: new Sort(0, Order.ASCENDING) ])

        when:
        underTest.addSortClassTo(table, options)

        then:
        table.select('.tableenhancer-sort-asc').size() == 1
        table.select('.tableenhancer-sort-desc').isEmpty()

        table.select('th:nth-child(1)').hasClass('tableenhancer-sort-asc')
    }

    def "'addSortClassTo' should add 'sortOrderThClass' for descending at column two if 'sortColumn' is '2' and 'DESCENDING'"() {

        given:
        def table = Tables.SIMPLE.firstTable
        Options options = optionsFor([ sort: new Sort(1, Order.DESCENDING) ])

        when:
        underTest.addSortClassTo(table, options)

        then:
        table.select('.tableenhancer-sort-asc').isEmpty()
        table.select('.tableenhancer-sort-desc').size() == 1

        table.select('th:nth-child(2)').hasClass('tableenhancer-sort-desc')
    }

    def "'addSortClassTo' should add 'sortOrderThClass' only at top level table"() {

        given:
        def table = Tables.WITH_INNER_TABLES.firstTable
        def options = optionsFor([ sort: new Sort(0, Order.ASCENDING) ])

        when:
        underTest.addSortClassTo(table, options)

        then:
        // => check outer table
        table.select('.tableenhancer-sort-asc').size() == 1
        table.select('.tableenhancer-sort-desc').isEmpty()

        table.select('th:nth-child(1)').hasClass('tableenhancer-sort-asc')

        // => check inner tables that they do have neither sorting nor row number enhancement
        table.select('td table.tableenhancer-sort-asc, td table.tableenhancer-sort-desc').isEmpty()
    }

    def "'sort' should not change anything if '#options'"() {

        given:
        def table = Tables.SIMPLE.firstTable

        when:
        underTest.sort(table, options)

        then:
        table == Tables.SIMPLE.firstTable

        where:
        options << [
                optionsFor([ deviceType: DeviceType.DESKTOP ]),
                optionsFor([ deviceType: DeviceType.MOBILE ]),

                optionsFor([ rowNumbers: RowNumbers.ONCE_BEFORE_SORTING ]),
                optionsFor([ rowNumbers: RowNumbers.INDEPENDENT_FROM_SORTING ]),

                optionsFor([ totalLine: new TotalLine(true, DecimalMark.POINT) ]),
                optionsFor([ totalLine: new TotalLine(true, DecimalMark.COMMA) ]),
        ]
    }

    def "'sort' should not throw 'IllegalArgumentException' if body contains no table but \"empty\" option"() {

        given:
        def table = Tables.SIMPLE.firstTable

        Options options = optionsFor([:])

        Element divElement = (Tables.NONE.content as Document).body().child(0)

        when:
        underTest.sort(divElement, options)

        then:
        noExceptionThrown()
    }

    def "'sort' should throw 'IllegalArgumentException' if body contains no table"() {

        given:
        def divElement = (Tables.NONE.content as Document).body().child(0)
        def options = optionsFor([ sort: new Sort(0, Order.ASCENDING) ])

        when:
        underTest.sort(divElement, options)

        then:
        thrown IllegalArgumentException
    }

    def "'sort' should not throw 'MacroExecutionException' if table has no header but \"empty\" options"() {

        given:
        def table = Tables.WITHOUT_HEADING.firstTable
        def options = optionsFor([:])

        when:
        underTest.sort(table, options)

        then:
        table.toString() == Tables.WITHOUT_HEADING.firstTable.toString()

    }

    def "'sort' should throw 'MacroExecutionException' if table has no header"() {

        given:
        def table = Tables.WITHOUT_HEADING.firstTable
        def options = optionsFor([ sort: new Sort(1, Order.ASCENDING) ])

        when:
        underTest.sort(table, options)

        then:
        thrown MacroExecutionException
    }

    def "'sort' should throw 'MacroExecutionException' if 'sortColumn' is negative"() {

        given:
        def body = Tables.SIMPLE.firstTable
        def options = optionsFor([ sort: new Sort(-1, Order.DESCENDING) ])

        when:
        underTest.sort(body, options)

        then:
        thrown MacroExecutionException
    }

    def "'sort' should throw 'MacroExecutionException' if 'sortColumn' is greater than columns count"() {

        given:
        def body = Tables.SIMPLE.firstTable
        def options = optionsFor([ sort: new Sort(3, Order.ASCENDING) ])

        when:
        underTest.sort(body, options)

        then:
        thrown MacroExecutionException
    }

    def "'sort' should sort by column one if 'sortColumn' is '1'"() {

        given:
        def table = Tables.SIMPLE.firstTable
        def options = optionsFor([ sort: new Sort(0, Order.ASCENDING) ])

        when:
        underTest.sort(table, options)

        then:
        table.$('tr:nth-child(1) > th') == elemConfThsWith('str', 'int', 'float')
        table.$('tr:nth-child(2) > td') == elemConfTdsWith('four', '4', '4.04')
        table.$('tr:nth-child(3) > td') == elemConfTdsWith('one', '1', '1.01')
        table.$('tr:nth-child(4) > td') == elemConfTdsWith('three', '3', '3.03')
        table.$('tr:nth-child(5) > td') == elemConfTdsWith('two', '2', '2.02')
    }

    def "'sort' should sort by column two descending if 'sortColumn' is '2' and 'DESCENDING"() {

        given:
        def table = Tables.SIMPLE.firstTable
        def options = optionsFor([ sort: new Sort(1, Order.DESCENDING) ])

        when:
        underTest.sort(table, options)

        then:
        table.$('tr:nth-child(1) > th') == elemConfThsWith('str', 'int', 'float')
        table.$('tr:nth-child(2) > td') == elemConfTdsWith('four', '4', '4.04')
        table.$('tr:nth-child(3) > td') == elemConfTdsWith('three', '3', '3.03')
        table.$('tr:nth-child(4) > td') == elemConfTdsWith('two', '2', '2.02')
        table.$('tr:nth-child(5) > td') == elemConfTdsWith('one', '1', '1.01')
    }

    def "'sort' should add 'sortOrderThClass' only at top level table"() {

        given:
        def table = Tables.WITH_INNER_TABLES.firstTable
        def options = optionsFor([ sort: new Sort(0, Order.ASCENDING) ])


        when:
        underTest.sort(table, options)

        then:
        // => check outer table
        table.$('> tbody > tr:nth-child(1) > th') == elemConfThsWith('int', 'char', '?')
        table.$('> tbody > tr:nth-child(2) > td')[0..1] == elemConfTdsWith('1', 'b')
        table.$('> tbody > tr:nth-child(3) > td')[0..1] == elemConfTdsWith('2', 'd')
        table.$('> tbody > tr:nth-child(4) > td')[0..1] == elemConfTdsWith('3', 'a')
        table.$('> tbody > tr:nth-child(5) > td')[0..1] == elemConfTdsWith('4', 'c')

        // => check inner tables that they do have neither sorting nor row number enhancement
        table.$('tr:nth-child(2) > td:nth-child(3) > table > tbody > tr > *') == elemConfThsWith('e', 'f') + elemConfTdsWith('5', '6')
        table.$('tr:nth-child(3) > td:nth-child(3) > table > tbody > tr > *') == elemConfThsWith('g', 'h') + elemConfTdsWith('7', '8')
        table.$('tr:nth-child(4) > td:nth-child(3) > table > tbody > tr > *') == elemConfThsWith('a', 'b') + elemConfTdsWith('1', '2')
        table.$('tr:nth-child(5) > td:nth-child(3) > table > tbody > tr > *') == elemConfThsWith('c', 'd') + elemConfTdsWith('3', '4')
    }
}
