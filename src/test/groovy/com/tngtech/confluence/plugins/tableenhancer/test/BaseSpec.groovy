package com.tngtech.confluence.plugins.tableenhancer.test

import com.tngtech.confluence.plugins.tableenhancer.Options
import com.tngtech.confluence.plugins.tableenhancer.Options.DeviceType
import com.tngtech.confluence.plugins.tableenhancer.Options.RowNumbers
import com.tngtech.confluence.plugins.tableenhancer.Options.RowNumbersBgColor
import com.tngtech.confluence.plugins.tableenhancer.Options.Sort
import com.tngtech.confluence.plugins.tableenhancer.Options.TotalLine
import org.jsoup.Jsoup
import org.jsoup.nodes.Attribute
import org.jsoup.nodes.Attributes
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element
import org.jsoup.parser.Tag
import org.jsoup.select.Elements

import java.util.regex.Pattern

abstract class BaseSpec extends spock.lang.Specification {

    // String as Document
    static {
        def savedAsType = String.metaClass.getMetaMethod('asType', [ Class ] as Class[])
        String.metaClass.asType = { Class typeClass ->
            if (typeClass == Document) {
                return Jsoup.parseBodyFragment(delegate);
            }
            return savedAsType.invoke(delegate, [ typeClass ] as Object[])
        }
    }

    // Element(s).$ == Element(s).select
    static {
        Element.metaClass.$ = { String cssQuery ->
            return delegate.select(cssQuery)
        }
        Elements.metaClass.$ = { String cssQuery ->
            return delegate.select(cssQuery)
        }
    }

    // Element.equals
    static {
        def savedEquals = Element.metaClass.getMetaMethod('equals', [ Object ] as Class[])
        Element.metaClass.equals = { Object other ->
            if (savedEquals.invoke(delegate, [ other ] as Object[])) {
                return true;
            }
            // TODO better impl?
            return delegate.toString().replaceAll('" confluenceT([hd])', '"confluenceT$1') == other.toString()
        }
    }

    static def Pattern pattern = ~/elem(Conf)?(T[hd]s?)(.*?)(Empty|With)/

    def methodMissing(String name, args) {
        // TODO refactor!
        def matcher = pattern.matcher(name)
        if (matcher.matches()) {
            def isConf = matcher[0][1] as boolean
            def tag = matcher[0][2]
            def color = matcher[0][3]
            def isEmpty = matcher[0][4] == 'Empty'

            def newArgs = args as List

            if (tag ==~ /T[dh]s/) {
                return newArgs.collect{ String innerHtml ->
                    if (isEmpty) {
                        throw new MissingMethodException(name, this.class, args)
                    } else {
                        "elem${(isConf) ? 'Conf' : ''}${tag - 's'}${color}With"(innerHtml)
                    }
                }
            } else {
                def innerHtml = '&nbsp;'
                if (!isEmpty && newArgs[0] instanceof String) {
                    innerHtml = args[0]
                    newArgs.remove(0)
                }

                def attributes = [:]
                if (isConf) {
                    if (color) {
                        attributes += [
                                class                  : "confluence${tag} highlight-${color.toLowerCase()}",
                                'data-highlight-colour': color.toLowerCase(),
                            ]
                    } else {
                        attributes += [ class: "confluence${tag}" ]
                    }
                }
                if (!newArgs.isEmpty()) {
                    if (newArgs[0] instanceof Map) {
                        attributes += newArgs[0]
                    } else {
                        throw new MissingMethodException(name, this.class, args)
                    }
                }

                return elem(tag.toLowerCase(), innerHtml, attributes)
            }
        }
        throw new MissingMethodException(name, this.class, args)
    }

    /** @param attributes to be set at element (either sets new attribute, or replaces an existing one by key. */
    def Element elem(String tag, String innerHtml, Map<String, String> attributes) {
        Attributes attrs = new Attributes()
        attributes.each{ name, value ->
            attrs.put(new Attribute(name, value))
        }
        Element result = new Element(Tag.valueOf(tag), '', attrs)
        result.html(innerHtml)
        return result
    }

    def static Options optionsFor(Map<String, ?> options) {
        return new Options(
                options.deviceType ?: DeviceType.DESKTOP,
                options.rowNumbers ?: RowNumbers.NONE,
                options.rowNumbersBgColor ?: RowNumbersBgColor.GREY,
                options.sort ?: Sort.NONE,
                options.totalLine ?: TotalLine.NONE,
				options.numberOfFixedRows ?: 0,
				options.numberOfFixedColumns ?: 0,
                options.warnings ?: [],
            )
    }
}
