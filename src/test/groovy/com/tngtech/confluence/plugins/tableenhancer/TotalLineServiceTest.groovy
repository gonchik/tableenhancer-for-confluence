package com.tngtech.confluence.plugins.tableenhancer

import com.tngtech.confluence.plugins.tableenhancer.Options.DeviceType
import com.tngtech.confluence.plugins.tableenhancer.Options.RowNumbers
import com.tngtech.confluence.plugins.tableenhancer.Options.Sort
import com.tngtech.confluence.plugins.tableenhancer.Options.Sort.Order
import com.tngtech.confluence.plugins.tableenhancer.Options.TotalLine
import com.tngtech.confluence.plugins.tableenhancer.Options.TotalLine.DecimalMark
import com.tngtech.confluence.plugins.tableenhancer.test.BaseSpec
import com.tngtech.confluence.plugins.tableenhancer.test.Tables
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element
import org.jsoup.parser.Tag
import spock.lang.Unroll

class TotalLineServiceTest extends BaseSpec {

    private TotalLineService underTest

    private ConfluenceTableService confluenceTableService = Mock(){
        newTd(_) >> { new Element(Tag.valueOf('td'), '') }
    }

    public void setup() {
        underTest = new TotalLineService(confluenceTableService)
    }

    @Unroll
    def "'addTotalLineTo' should not change anything if '#options'"() {

        given:
        def table = Tables.SIMPLE.firstTable

        when:
        underTest.addTotalLineTo(table, options)

        then:
        table == Tables.SIMPLE.firstTable

        where:
        options << [
                optionsFor([ deviceType: DeviceType.DESKTOP ]),
                optionsFor([ deviceType: DeviceType.MOBILE ]),

                optionsFor([ numberOfFixedRows: 0 ]),
                optionsFor([ numberOfFixedColumns: 0 ]),

                optionsFor([ rowNumbers: RowNumbers.ONCE_BEFORE_SORTING ]),
                optionsFor([ rowNumbers: RowNumbers.ONCE_AFTER_SORTING ]),

                optionsFor([ sort: new Sort(0, Order.ASCENDING) ]),
                optionsFor([ sort: new Sort(1, Order.DESCENDING) ]),
        ]
    }

    def "'addTotalLineTo' should not throw 'IllegalArgumentException' if 'totalLine' is disabled even if element is no table"() {

        given:
        def divElement = (Tables.NONE.content as Document).body().child(0)
        def options = optionsFor([:])

        when:
        underTest.addTotalLineTo(divElement, options)

        then:
        noExceptionThrown()
    }

    def "'addTotalLineTo' should throw 'IllegalArgumentException' if element contains no table"() {

        given:
        def bodyElement = (Tables.NONE.content as Document).body()
        def options = optionsFor([ totalLine: new TotalLine(true, DecimalMark.POINT) ])

        when:
        underTest.addTotalLineTo(bodyElement, options)

        then:
        def e = thrown IllegalArgumentException
        e.message == 'Given element is not a table.'
    }


    def "'addTotalLineTo' should add sums for english formatted floats if 'totalLine' is enabled"() {

        given:
        def table = Tables.SIMPLE_NUMS_EN.firstTable
        def options = optionsFor([ totalLine: new TotalLine(true, DecimalMark.POINT) ])


        when:
        underTest.addTotalLineTo(table, options)

        then:
        table.select('tfoot > tr > td') == elemTdsWith('10.1', '10,010', '101,010.1', '1,011,011,011,011,010', '-16.25', '5')
    }

    def "'addTotalLineTo' should add sums for german formatted floats if 'totalLine' is enabled"() {

        given:
        def table = Tables.SIMPLE_NUMS_DE.firstTable
        def options = optionsFor([ totalLine: new TotalLine(true, DecimalMark.COMMA) ])

        when:
        underTest.addTotalLineTo(table, options)

        then:
        table.select('tfoot > tr > td') == elemTdsWith('10,1', '10.010', '101.010,1', '1.011.011.011.011.010', '-16,25', '5')
    }

    def "'addTotalLineTo' should not add sum to heading columns if 'totalLine' is enabled"() {

        given:
        def table = Tables.SIMPLE_NUMS_HEADING.firstTable
        def options = optionsFor([ totalLine: new TotalLine(true, DecimalMark.POINT) ])

        when:
        underTest.addTotalLineTo(table, options)

        then:
        table.select('tfoot > tr > td') == elemTdsWith('&nbsp;', '10')
    }

    def "'addTotalLineTo' should add sums if 'totalLine' is enabled but not for (faked) 'rowNumbers' column"() {

        given:
        def table = Tables.SIMPLE_NUMS_EN.firstTable
        def options = optionsFor([
                rowNumbers: RowNumbers.INDEPENDENT_FROM_SORTING,
                totalLine : new TotalLine(true, DecimalMark.POINT)
        ])

        table.addClass(options.getRowNumbersTableClass())

        when:
        underTest.addTotalLineTo(table, options)

        then:
        table.select('tfoot > tr > td') == elemTdsWith('&nbsp;', '10,010', '101,010.1', '1,011,011,011,011,010', '-16.25', '5')
    }

    def "'addTotalLineTo' should only add sums to top level table if 'totalLine' is enabled"() {

        given:
        def table = Tables.WITH_INNER_TABLES.firstTable
        def options = optionsFor([ totalLine: new TotalLine(true, DecimalMark.POINT) ])

        when:
        underTest.addTotalLineTo(table, options)

        then:
        table.select('tfoot').size() == 1
        table.select('tfoot > tr').size() == 1

        table.select('tfoot > tr > td') == elemTdsWith('10', '&nbsp;', '&nbsp;')

        // => check inner tables that they do have neither sorting nor row number enhancement
        !table.select('td > table').contains('tfoot')
        table.select('td > table th').size() == 4 * 2
        table.select('td > table td').size() == 4 * 2
    }

    def "'addTotalLineTo' should sum up on tables with merged cells correctly"() {

        given:
        def table = Tables.WITH_MERGED_CELLS.firstTable
        def options = optionsFor([ totalLine: new TotalLine(true, DecimalMark.POINT) ])

        when:
        underTest.addTotalLineTo(table, options)

        then:
        table.select('tfoot').size() == 1
        table.select('tfoot > tr').size() == 1

        table.select('tfoot > tr > td') == elemTdsWith('1', '15', '17')
    }

    def "'addTotalLineTo' should just add total line even on table without heading"() {

        given:
        def table = Tables.WITHOUT_HEADING.firstTable
        def options = optionsFor([ totalLine: new TotalLine(true, DecimalMark.POINT) ])

        when:
        underTest.addTotalLineTo(table, options)

        then:
        table.select('tfoot > tr > td') == elemTdsWith('&nbsp;', '6', '&nbsp;')
    }

    def "'parseNumber' should throw '#expected' for #str and 'commaAsDecimalMark' is '#commaAsDecimalMark'"() {

        given:

        when:
        underTest.parseNumber(str, commaAsDecimalMark)

        then:
        thrown expected

        where:
        str          | commaAsDecimalMark | expected
        null         | false              | NullPointerException
        null         | true               | NullPointerException
        'a'          | false              | NumberFormatException
        'a'          | true               | NumberFormatException

        '1l'         | false              | NumberFormatException
        '1l'         | true               | NumberFormatException

        '2 345'      | false              | NumberFormatException
        '2 345'      | true               | NumberFormatException

        '55.555.555' | false              | NumberFormatException
        '55,555,555' | true               | NumberFormatException
    }

    def "'parseNumber' should return '#expected' for #str and 'commaAsDecimalMark' is '#commaAsDecimalMark'"() {

        given:

        when:
        double result = underTest.parseNumber(str, commaAsDecimalMark)

        then:
        result == expected

        where:
        str                   | commaAsDecimalMark | expected
        ''                    | false              |                    0
        ''                    | true               |                    0

        ' '                   | false              |                    0
        ' '                   | true               |                    0

        '\u00a0'              | false              |                    0
        '\u00a0'              | true               |                    0

        '0'                   | false              |                    0
        '0'                   | true               |                    0

        '00'                  | false              |                    0
        '00'                  | true               |                    0

        ' 1'                  | false              |                    1
        ' 1'                  | true               |                    1
        '1 '                  | false              |                    1
        '1 '                  | true               |                    1
        '\u00a01'             | false              |                    1
        '\u00a01'             | true               |                    1
        '1\u00a0'             | false              |                    1
        '1\u00a0'             | true               |                    1

        '01'                  | false              |                    1
        '10'                  | false              |                   10

        '1'                   | false              |                    1
        '01'                  | false              |                    1
        '10'                  | false              |                   10

        '2020202020202020202' | false              | 2020202020202020202l
        '2020202020202020202' | true               | 2020202020202020202l

        '0.3'                 | false              |                    0.3
        '0.3'                 | true               |                    3
        '0,3'                 | false              |                    3
        '0,3'                 | true               |                    0.3

        '4,000.004'           | false              |                 4000.004
        '4,000.004'           | true               |                    4.000004
        '4.000,004'           | false              |                    4.000004
        '4.000,004'           | true               |                 4000.004

        '55.555.555'          | true               |             55555555
        '55,555,555'          | false              |             55555555
    }
}
