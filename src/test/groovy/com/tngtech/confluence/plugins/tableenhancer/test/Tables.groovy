package com.tngtech.confluence.plugins.tableenhancer.test

import org.jsoup.nodes.Document
import org.jsoup.nodes.Element
import org.junit.Assert


enum Tables {
    NONE,
    SIMPLE,
    COLORED,

    SIMPLE_NUMS_EN,
    SIMPLE_NUMS_DE,
    SIMPLE_NUMS_HEADING,

    SIMPLE_NOT_WRAPPED,
    SIMPLE_FURTHER_WRAPPED,

    MULTIPLE_TOP_LEVEL,
    WITH_INNER_TABLES,

    WITH_MERGED_CELLS,

    WITHOUT_HEADING,

    SIMPLE_UNSORTED_ROWNUMS,


    private static final String PREFIX = '/tables/'
    private static final String POSTFIX = '.html'

    String getContent() {
        if (this == NONE) {
            return '<div class=\"content\">123</div>'
        }

        URL url = this.getClass().getResource(PREFIX + this.name().toLowerCase() + POSTFIX)
        try {
            return new File(url.getFile()).text

        } catch (IOException e) {
            Assert.fail("Unable to open file from ${url}", e)
        }
    }

    Element getFirstTable() {
        if (this == NONE) {
            throw new UnsupportedOperationException('getFirstTable() cannot be called for Tables.NONE')
        }
        return (content as Document).body().select('table')[0]
    }
}
