package com.tngtech.confluence.plugins.tableenhancer

import com.atlassian.confluence.content.render.xhtml.ConversionContext
import com.atlassian.confluence.macro.MacroExecutionException
import com.tngtech.confluence.plugins.tableenhancer.test.BaseSpec


class TableenhancerMacroTest extends BaseSpec {

    private TableenhancerMacro underTest

    private EnhancerService enhancerService = Mock()
    private OptionService optionService = Mock()

    private ConversionContext conversionContext = Mock()

    def setup() throws Exception {
        underTest = new TableenhancerMacro(enhancerService, optionService)
    }

    def "'execute' does not catch 'MacroExecutionException' of 'EnhancerService.renderMacroDefiniton'"() {

        given:
        def parameters = [:]
        def body = '<div class="table-wrap">...</div>'

        when:
        underTest.execute(parameters, body, conversionContext)

        then:
        thrown MacroExecutionException

        then: 1 * optionService.parse(parameters, conversionContext) >> optionsFor([ warnings: [ 'Warning' ]])
        then: 1 * enhancerService.createRenderedWarning(_ as String, conversionContext) >> { throw new MacroExecutionException() }
        then: 0 * _._
    }

    def "'execute' does not catch 'MacroExecutionException' of 'EnhancerService.enhanceTable'"() {

        given:
        def parameters = [:]
        def body = '<div class="table-wrap">...</div>'

        when:
        underTest.execute(parameters, body, conversionContext)

        then:
        thrown MacroExecutionException

        then: 1 * optionService.parse(parameters, conversionContext) >> optionsFor([ warnings: [ 'Warning' ]])
        then: 1 * enhancerService.createRenderedWarning(_ as String, conversionContext) >> '<div class="warning" />'
        then: 1 * enhancerService.prepareWebResourcesFor(_ as Options)
        then: 1 * enhancerService.enhanceTables(_ as Options, _ as String, conversionContext) >> { throw new MacroExecutionException() }
        then: 0 * _._
    }

    def "'execute' calls methods correctly without warnings"() {

        given:
        def parameters = [:]
        def body = '<div class="table-wrap">...</div>'

        when:
        String result = underTest.execute(parameters, body, conversionContext)

        then:
        result == body

        then: 1 * optionService.parse(parameters, conversionContext) >> optionsFor([:])
        then: 1 * enhancerService.prepareWebResourcesFor(_ as Options)
        then: 1 * enhancerService.enhanceTables(_ as Options, body, conversionContext) >> { option, string, cc -> string }
        then: 0 * _._
    }

    def "'execute' calls methods correctly with one warning"() {

        given:
        def parameters = [:]
        def body = '<div class="table-wrap">...</div>'

        def warningAsString = '<div class="warning" />'

        when:
        String result = underTest.execute(parameters, body, conversionContext)

        then:
        result == warningAsString + body

        then: 1 * optionService.parse(parameters, conversionContext) >> optionsFor([ warnings: [ 'Warning' ]])
        then: 1 * enhancerService.createRenderedWarning('Warning', conversionContext) >> warningAsString
        then: 1 * enhancerService.prepareWebResourcesFor(_ as Options)
        then: 1 * enhancerService.enhanceTables(_ as Options, { it.contains(body) }, conversionContext) >> {
                option, string, cc -> string
            }
        then: 0 * _._
    }

    def "'execute' calls methods correctly with multiple warnings"() {

        given:
        def parameters = [:]
        def body = '<div class="table-wrap">...</div>'

        String warningAsString = '<div class="warning" />'

        when:
        String result = underTest.execute(parameters, body, conversionContext)

        then:
        result == warningAsString + warningAsString + warningAsString + body

        then: 1 * optionService.parse(parameters, conversionContext) >> optionsFor([ warnings: [ 'Warn1', 'Warn2', 'Warn3' ] ])
        then: 3 * enhancerService.createRenderedWarning(_ as String, conversionContext) >> warningAsString
        then: 1 * enhancerService.prepareWebResourcesFor(_ as Options)
        then: 1 * enhancerService.enhanceTables(_ as Options, { it.contains(body) }, conversionContext) >> {
            option, string, cc -> string
        }
        then: 0 * _._
    }
}
