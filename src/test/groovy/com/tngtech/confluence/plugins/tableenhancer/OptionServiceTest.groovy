package com.tngtech.confluence.plugins.tableenhancer

import com.atlassian.confluence.content.render.xhtml.ConversionContext
import com.tngtech.confluence.plugins.tableenhancer.Options.DeviceType
import com.tngtech.confluence.plugins.tableenhancer.Options.RowNumbers
import com.tngtech.confluence.plugins.tableenhancer.Options.RowNumbersBgColor
import com.tngtech.confluence.plugins.tableenhancer.test.BaseSpec
import spock.lang.Unroll

class OptionServiceTest extends BaseSpec {

    private OptionService underTest

    private I18nService i18nService = Mock()

    private ConversionContext conversionContext = Mock()

    def setup() {
        underTest = new OptionService(i18nService)
    }

    @Unroll
    def "'parse' should return deviceType #expectedDeviceType from #deviceTypeProperty"() {

        given:
        conversionContext.getPropertyAsString('output-device-type') >> { deviceTypeProperty }


        when:
        Options result = underTest.parse([ : ], conversionContext)

        then:
        result.deviceType == expectedDeviceType
        result.getWarningMessages().isEmpty()

        0 * i18nService._

        where:
        deviceTypeProperty | expectedDeviceType
        null               | DeviceType.DESKTOP
        ''                 | DeviceType.DESKTOP
        'none'             | DeviceType.DESKTOP
        'desktop'          | DeviceType.DESKTOP
        'mobile'           | DeviceType.MOBILE
    }


    @Unroll
    def "'parse' should return rowNumbers #expectedRowNumbers from #params"() {

        given:

        when:
        Options result = underTest.parse(params, conversionContext)

        then:
        result.rowNumbers == expectedRowNumbers
        result.getWarningMessages().isEmpty()

        0 * i18nService._

        where:
        params                                   | expectedRowNumbers
        [ rowNumbers: null ]                     | RowNumbers.NONE
        [ rowNumbers: '' ]                       | RowNumbers.NONE
        [ rowNumbers: 'none' ]                   | RowNumbers.NONE
        [ rowNumbers: 'onceBeforeSorting' ]      | RowNumbers.ONCE_BEFORE_SORTING
        [ rowNumbers: 'independentFromSorting' ] | RowNumbers.INDEPENDENT_FROM_SORTING
        [ rowNumbers: 'onceAfterSorting' ]       | RowNumbers.ONCE_AFTER_SORTING

        // Compatibility to v1.0
        [ autoNumber: 'false' ] | RowNumbers.NONE
        [ autoNumber: 'true' ] | RowNumbers.TRUE

        // Compatibility to v2.5
        [ autoNumber: 'none' ] | RowNumbers.NONE
        [ autoNumber: 'onceBeforeSorting' ] | RowNumbers.ONCE_BEFORE_SORTING
        [ autoNumber: 'independentFromSorting' ] | RowNumbers.INDEPENDENT_FROM_SORTING
        [ autoNumber: 'onceAfterSorting' ] | RowNumbers.ONCE_AFTER_SORTING
    }

    @Unroll
    def "'parse should return rowNumbersBgColor #expectedRowNumbersBgColor from #params"() {

        given:

        when:
        Options result = underTest.parse(params, conversionContext)

        then:
        result.@rowNumbersBgColor == expectedRowNumbersBgColor
        result.getWarningMessages().isEmpty()

        0 * i18nService._

        where:
        params                                   | expectedRowNumbersBgColor
        [ rowNumbersBgColor: null ]              | RowNumbersBgColor.GREY
        [ rowNumbersBgColor: '' ]                | RowNumbersBgColor.GREY
        [ rowNumbersBgColor: 'grey' ]            | RowNumbersBgColor.GREY
        [ rowNumbersBgColor: 'red' ]             | RowNumbersBgColor.RED
        [ rowNumbersBgColor: 'green' ]           | RowNumbersBgColor.GREEN
        [ rowNumbersBgColor: 'blue' ]            | RowNumbersBgColor.BLUE
        [ rowNumbersBgColor: 'yellow' ]          | RowNumbersBgColor.YELLOW
        [ rowNumbersBgColor: 'transparent' ]     | RowNumbersBgColor.TRANSPARENT
        [ rowNumbersBgColor: 'asRightNextCell' ] | RowNumbersBgColor.AS_RIGHT_NEXT_CELL
    }

    @Unroll
    def "'parse' should add warning with #expectedSortColumn if colIndex is not parsable from #params"() {

        given:

        when:
        Options result = underTest.parse(params, conversionContext)

        then:
        result
        !result.sortTable()
        result.getWarningMessages().size() == 1

        1 * i18nService.sortColumnNotParsableWarning(expectedSortColumn)
        0 * i18nService._

        where:
        params                 | expectedSortColumn
        [ sortColumn: '' ]     | ''
        [ sortColumn: 'none' ] | 'none'
        [ sortColumn: '1a' ]   | '1a'
    }

    @Unroll
    def "'parse' should return colIndex #expectedColIndex and sortDecending #expectedSortDescending from #params"() {

        given:

        when:
        Options result = underTest.parse(params, conversionContext)

        then:
        result.sortTable()
        result.sortColumnIndex() == expectedColIndex
        result.sortDescending() == expectedSortDescending
        result.getWarningMessages().isEmpty()
        0 * i18nService._

        where:
        params                                       | expectedColIndex | expectedSortDescending
        [ sortColumn: '-1' ]                         | -2               | false
        [ sortColumn: '0' ]                          | -1               | false
        [ sortColumn: '1' ]                          | 0                | false
        [ sortColumn: '3' ]                          | 2                | false
        [ sortColumn: '10' ]                         | 9                | false

        [ sortColumn: '0', sortDescending: '' ]      | -1               | false
        [ sortColumn: '1', sortDescending: '' ]      | 0                | false

        [ sortColumn: '0', sortDescending: 'false' ] | -1               | false
        [ sortColumn: '1', sortDescending: 'false' ] | 0                | false

        [ sortColumn: '0', sortDescending: 'true' ]  | -1               | true
        [ sortColumn: '1', sortDescending: 'true' ]  | 0                | true
    }

    @Unroll
    def "'parse' should return number of fixed rows #fixedRows and columns #fixedCols from #params"() {

        given:

        when:
        Options result = underTest.parse(params, conversionContext)

        then:
        result.getNumberOfFixedRows() == fixedRows
        result.getNumberOfFixedColumns() == fixedCols
        result.getWarningMessages().isEmpty()
        0 * i18nService._

        where:
        params                                                  | fixedRows | fixedCols
        [ numberOfFixedRows: null, numberOfFixedColumns: null ] | 0         | 0
        [ numberOfFixedRows: '-1', numberOfFixedColumns: '0' ]  | -1        | 0
        [ numberOfFixedRows: '2', numberOfFixedColumns: '1' ]   | 2         | 1
        [ numberOfFixedRows: '7', numberOfFixedColumns: null ]  | 7         | 0
    }

    def "'parse' should add warning with #expectedFixedRows if numberOfFixedRows is not parsable from #params"() {

        given:

        when:
        Options result = underTest.parse(params, conversionContext)

        then:
        result
        result.getNumberOfFixedRows() == 0
        result.getWarningMessages().size() == 1

        1 * i18nService.numberOfFixedRowsNotParsableWarning(expectedFixedRows)
        0 * i18nService._

        where:
        params                        | expectedFixedRows
        [ numberOfFixedRows: '' ]     | ''
        [ numberOfFixedRows: 'none' ] | 'none'
        [ numberOfFixedRows: '1a' ]   | '1a'
    }

    def "'parse' should add warning with #expectedFixedCols if numberOfFixedColumns is not parsable from #params"() {

        given:

        when:
        Options result = underTest.parse(params, conversionContext)

        then:
        result
        result.getNumberOfFixedRows() == 0
        result.getWarningMessages().size() == 1

        1 * i18nService.numberOfFixedColumnsNotParsableWarning(expectedFixedCols)
        0 * i18nService._

        where:
        params                           | expectedFixedCols
        [ numberOfFixedColumns: '' ]     | ''
        [ numberOfFixedColumns: 'none' ] | 'none'
        [ numberOfFixedColumns: '1a' ]   | '1a'
    }

    def "'parse' should not set sortTable option to true if only sortDescending is supplied"() {

        given:

        when:
        Options result = underTest.parse([ sortDescending: 'true' ], conversionContext)

        then:
        !result.sortTable()
        result.getWarningMessages().isEmpty()
        0 * i18nService._
    }

    @Unroll
    def "'parse' should return hasTotalLine #expectedHasTotalLine and commaAsDecimalMark #expectedCommaAsDecimalMark from #params"() {

        given:

        when:
        Options result = underTest.parse(params, conversionContext)

        then:
        result.hasTotalLine() == expectedHasTotalLine
        result.useCommaAsDecimalMark() == expectedCommaAsDecimalMark
        result.getWarningMessages().isEmpty()
        0 * i18nService._

        where:
        params                                          | expectedHasTotalLine | expectedCommaAsDecimalMark
        [ totalLine: null ]                             | false                | false
        [ totalLine: '' ]                               | false                | false
        [ totalLine: 'none' ]                           | false                | false
        [ totalLine: 'true' ]                           | true                 | false

        [ totalLine: 'true', decimalMark: null ]        | true                 | false
        [ totalLine: 'true', decimalMark: '' ]          | true                 | false
        [ totalLine: 'true', decimalMark: 'a' ]         | true                 | false
        [ totalLine: 'true', decimalMark: '. (point)' ] | true                 | false
        [ totalLine: 'true', decimalMark: ', (comma)' ] | true                 | true

    }

    def "'parse' should return correctly parsed options"() {

        given:
        def params = [
                rowNumbers          : 'onceBeforeSorting',
                sortColumn          : '1',
                sortDescending      : 'true',
                totalLine           : 'true',
                numberOfFixedRows   : '1',
                numberOfFixedColumns: '2',
        ]
        conversionContext.getPropertyAsString('output-device-type') >> 'mobile'

        when:
        Options result = underTest.parse(params, conversionContext)

        then:
        result.deviceType == DeviceType.MOBILE
        result.getNumberOfFixedRows() == 1
        result.getNumberOfFixedColumns() == 2
        result.rowNumbers == RowNumbers.ONCE_BEFORE_SORTING
        result.sortTable()
        result.sortColumnIndex() == 1
        result.sortDescending()
        result.hasTotalLine()
        !result.useCommaAsDecimalMark()
        result.getWarningMessages().isEmpty()
    }
}
