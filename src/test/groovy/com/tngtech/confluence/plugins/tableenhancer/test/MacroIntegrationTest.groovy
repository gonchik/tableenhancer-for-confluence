package com.tngtech.confluence.plugins.tableenhancer.test

import com.atlassian.confluence.content.render.xhtml.ConversionContext
import com.atlassian.confluence.util.i18n.I18NBean
import com.atlassian.confluence.util.i18n.I18NBeanFactory
import com.atlassian.confluence.xhtml.api.XhtmlContent
import com.atlassian.plugin.webresource.WebResourceManager
import com.tngtech.confluence.plugins.tableenhancer.ConfluenceTableService
import com.tngtech.confluence.plugins.tableenhancer.EnhancerService
import com.tngtech.confluence.plugins.tableenhancer.FixRowColService
import com.tngtech.confluence.plugins.tableenhancer.I18nService
import com.tngtech.confluence.plugins.tableenhancer.OptionService
import com.tngtech.confluence.plugins.tableenhancer.RowNumberService
import com.tngtech.confluence.plugins.tableenhancer.SorterService
import com.tngtech.confluence.plugins.tableenhancer.TableenhancerMacro
import com.tngtech.confluence.plugins.tableenhancer.TotalLineService
import org.jsoup.nodes.Document
import org.jsoup.select.Elements

class MacroIntegrationTest extends BaseSpec {

    private TableenhancerMacro underTest

    private I18NBeanFactory i18nBeanFactory = Mock(){
        getI18NBean() >> { i18nBean }
    }
    private I18NBean i18nBean = Mock(){
        getText(_, _) >> { key, args -> key }
    }
    private WebResourceManager webResourceManager = Mock()
    private XhtmlContent xhtmlContent = Mock()

    private ConversionContext conversionContext = Mock()

    def setup() {
        // Create service structure manually
        FixRowColService fixRowColService = new FixRowColService()
        I18nService i18nService = new I18nService(i18nBeanFactory)
        ConfluenceTableService confluenceTableService = new ConfluenceTableService()
        RowNumberService rowNumberService = new RowNumberService(confluenceTableService)
        SorterService sorterService = new SorterService(i18nService)
        TotalLineService totalLineService = new TotalLineService(confluenceTableService)
        EnhancerService enhancerService = new EnhancerService(fixRowColService, i18nService, rowNumberService,
                sorterService, totalLineService, webResourceManager, xhtmlContent)
        OptionService optionService = new OptionService(i18nService)

        underTest = new TableenhancerMacro(enhancerService, optionService)
    }

    def "tableenhancer should just add class to table if no parameters are specified"() {

        given:
        def params = [ : ]
        def body = Tables.SIMPLE.content

        when:
        String result = underTest.execute(params, body, conversionContext)

        then:
        def r = result as Document
        r.$("table.confluenceTable.tableenhancer").size() == 1

        r.$('table.confluenceTable.tableenhancer').removeClass('tableenhancer')
        r == body as Document
    }

    def "tableenhancer should add 'rowNumbers' and 'sort' for simple table on desktop"() {

        given:
        def params = [ rowNumbers: 'independentFromSorting', sortColumn: '2' ]
        def body = Tables.SIMPLE.content
        conversionContext.getPropertyAsString('output-device-type') >> 'desktop'

        when:
        String result = underTest.execute(params, body, conversionContext)

        then:
        def r = result as Document

        r.$('table.confluenceTable.tableenhancer.tableenhancer-rownums-independentFromSorting').size() == 1

        r.$('.tableenhancer-sort-asc').size() == 1
        r.$('.tableenhancer-sort-desc').isEmpty()
        r.$('th:nth-child(3)').hasClass('tableenhancer-sort-asc')

        r.$('tr > th:first-child') == [ elemConfThEmpty([ 'data-sorter': 'false' ]) ]
        r.$('tr > td:first-child') == elemConfTdsGreyWith('1', '2', '3', '4')
    }

    def "tableenhancer should add 'rowNumbers' and 'sort' for simple table on mobile"() {

        given:
        def params = [ rowNumbers: 'onceAfterSorting', sortColumn: '1' ]
        def body = Tables.SIMPLE.content
        conversionContext.getPropertyAsString('output-device-type') >> 'mobile'

        when:
        String result = underTest.execute(params, body, conversionContext)

        then:
        def r = result as Document
        r.$('table.confluenceTable.tableenhancer.tableenhancer-rownums-onceAfterSorting').size() == 1

        r.$('.tableenhancer-sort-asc').size() == 1
        r.$('.tableenhancer-sort-desc').isEmpty()
        r.$('th:nth-child(2)').hasClass('tableenhancer-sort-asc')

        r.$('tr:nth-child(1) > th') == [
                elemConfThEmpty(),
                elemConfThWith('str', [ class: 'confluenceTh tableenhancer-sort-asc' ]),
                elemConfThWith('int'),
                elemConfThWith('float')
            ]
        r.$('tr:nth-child(2) > td') == elemConfTdsGreyWith('1') + elemConfTdsWith('four',  '4', '4.04')
        r.$('tr:nth-child(3) > td') == elemConfTdsGreyWith('2') + elemConfTdsWith('one',   '1', '1.01')
        r.$('tr:nth-child(4) > td') == elemConfTdsGreyWith('3') + elemConfTdsWith('three', '3', '3.03')
        r.$('tr:nth-child(5) > td') == elemConfTdsGreyWith('4') + elemConfTdsWith('two',   '2', '2.02')
    }

    def "tableenhancer should add 'rowNumbers' and 'sort' only on top level table"() {

        given:
        def params = [ rowNumbers: 'onceAfterSorting', sortColumn: '1', sortDescending: 'true' ]
        def body = Tables.WITH_INNER_TABLES.content

        when:
        String result = underTest.execute(params, body, conversionContext)

        then:
        def r = result as Document

        r.$('table.confluenceTable.tableenhancer.tableenhancer-rownums-onceAfterSorting').size() == 1

        r.$('.tableenhancer-sort-asc').isEmpty()
        r.$('.tableenhancer-sort-desc').size() == 1
        r.$('th:nth-child(2)').hasClass('tableenhancer-sort-desc')

        // => check outer table
        r.$('body > div > table > tbody > tr > th:first-child') == [ elemConfThEmpty() ]
        r.$('body > div > table > tbody > tr > td:first-child') == elemConfTdsGreyWith('1', '2', '3', '4')

        // => check inner tables that they do have neither sorting nor row number enhancement
        Elements innerTables = r.$('table table')

        innerTables.$('.tableenhancer-sort-asc, .tableenhancer-sort-desc').isEmpty()

        innerTables.$('th').size() == 4 * 2
        innerTables.$('td').size() == 4 * 2
    }

    def "tableenhancer should add 'rowNumbers' and 'sort' on every top level table"() {

        given:
        def params = [ rowNumbers: 'onceBeforeSorting', sortColumn: '2', sortDescending: 'false' ]
        def body = Tables.MULTIPLE_TOP_LEVEL.content

        when:
        String result = underTest.execute(params, body, conversionContext)

        then:
        def r = result as Document

        r.$('table.confluenceTable.tableenhancer.tableenhancer-rownums-onceBeforeSorting').size() == 2

        r.$('.tableenhancer-sort-asc').size() == 2
        r.$('.tableenhancer-sort-desc').isEmpty()
        r.$('th:nth-child(3)').hasClass('tableenhancer-sort-asc')

        r.$('div:nth-child(1) tr > th:first-child') == [ elemConfThEmpty() ]
        r.$('div:nth-child(1) tr > td:first-child') == elemConfTdsGreyWith('1', '2')

        r.$('div:nth-child(2) tr > th:first-child') == [ elemConfThEmpty() ]
        r.$('div:nth-child(2) tr > td:first-child') == elemConfTdsGreyWith('1', '2', '3')
    }

    def "tableenhancer should add 'rowNumbers' and 'totalLine' for simple table on desktop"() {

        given:
        def params = [ rowNumbers: 'onceBeforeSorting', totalLine: 'true', decimalMark: ',' ]
        def body = Tables.SIMPLE_NUMS_EN.content
        conversionContext.getPropertyAsString('output-device-type') >> 'desktop'

        when:
        String result = underTest.execute(params, body, conversionContext)

        then:
        def r = result as Document

        r.$('table.tableenhancer.tableenhancer-rownums-onceBeforeSorting').size() == 1

        r.$('.tableenhancer-sort-asc').isEmpty()
        r.$('.tableenhancer-sort-desc').isEmpty()

        r.$('tr > th:first-child') == [ elemConfThEmpty() ]
        r.$('tr > td:first-child') == elemConfTdsGreyWith('1', '2', '3', '4', '&nbsp')
        r.$('tfoot > tr > td') == elemConfTdsGreyWith('&nbsp;', '10.1', '10,010', '101,010.1', '1,011,011,011,011,010', '-16.25', '5')
    }

    def "tableenhancer should add 'rowNumbers' and 'totalLine' on table without heading"() {


        given:
        def params = [ rowNumbers: 'independentFromSorting', totalLine: 'true' ]
        def body = Tables.WITHOUT_HEADING.content

        when:
        String result = underTest.execute(params, body, conversionContext)

        then:
        def r = result as Document

        r.$('tbody > tr > td:first-child') == elemConfTdsGreyWith('1', '2', '3')
        r.$('tfoot > tr > td') == elemConfTdsGreyWith('&nbsp;', '&nbsp;', '6', '&nbsp;')
    }

    def "tableenhancer should use 'rowNumbersBgColor == AS_RIGHT_NEXT_CELL' for manually colored table"() {

        given:
        def params = [ rowNumbers: 'onceAfterSorting', rowNumbersBgColor: 'asRightNextCell' ]
        def body = Tables.COLORED.content
        conversionContext.getPropertyAsString('output-device-type') >> 'desktop'

        when:
        String result = underTest.execute(params, body, conversionContext)

        then:
        Document r = result as Document

        r.$('tr:nth-child(1) > th:first-child') == [ elemConfThBlueEmpty() ]
        r.$('tr:nth-child(2) > td:first-child') == [ elemConfTdGreenWith('1') ]
    }

    def "tableenhancer should use 'rowNumbersBgColor == TRANSPARENT' for manually colored table"() {

        given:
        def params = [ rowNumbers: 'onceAfterSorting', rowNumbersBgColor: 'transparent' ]
        def body = Tables.COLORED.content
        conversionContext.getPropertyAsString('output-device-type') >> 'desktop'

        when:
        String result = underTest.execute(params, body, conversionContext)

        then:
        Document r = result as Document

        r.$('tr:nth-child(1) > th:first-child') == [ elemConfThBlueEmpty() ]
        r.$('tr:nth-child(2) > td:first-child') == [ elemConfTdWith('1') ]
    }

    def "tableenhancer should use 'rowNumbersBgColor' == YELLOW for manually colored table"() {

        given:
        def params = [ rowNumbers: 'onceAfterSorting', rowNumbersBgColor: 'yellow' ]
        def body = Tables.COLORED.content
        conversionContext.getPropertyAsString('output-device-type') >> 'desktop'

        when:
        String result = underTest.execute(params, body, conversionContext)

        then:
        Document r = result as Document

        r.$('tr:nth-child(1) > th:first-child') == [ elemConfThBlueEmpty() ]
        r.$('tr:nth-child(2) > td:first-child') == [ elemConfTdYellowWith('1') ]
    }
}
