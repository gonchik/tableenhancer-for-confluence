package com.tngtech.confluence.plugins.tableenhancer;

import java.util.Map;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.macro.annotation.Format;
import com.atlassian.confluence.content.render.xhtml.macro.annotation.RequiresFormat;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;

public class TableenhancerMacro implements Macro {

    private final EnhancerService enhancerService;
    private final OptionService optionService;

    public TableenhancerMacro(EnhancerService enhancerService, OptionService optionService) {
        this.enhancerService = enhancerService;
        this.optionService = optionService;
    }

    @Override
    @RequiresFormat(Format.View)
    public String execute(Map<String, String> parameters, String body, final ConversionContext conversionContext)
            throws MacroExecutionException {

        Options options = optionService.parse(parameters, conversionContext);
        StringBuilder bodyBuilder = new StringBuilder(body);
        for (String warningMessage : options.getWarningMessages()) {
            bodyBuilder.insert(0, enhancerService.createRenderedWarning(warningMessage, conversionContext));
        }
        body = bodyBuilder.toString();
        enhancerService.prepareWebResourcesFor(options);

        return enhancerService.enhanceTables(options, body, conversionContext);
    }

    @Override
    public BodyType getBodyType() {
        return BodyType.RICH_TEXT;
    }

    @Override
    public OutputType getOutputType() {
        return OutputType.BLOCK;
    }
}
