package com.tngtech.confluence.plugins.tableenhancer;

import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.google.common.annotations.VisibleForTesting;

public class I18nService {

    private static final String KEY_I18N_WARNING_SORT_COLUMN_INDEX_NOT_PARSEABLE = "com.tngtech.confluence.plugins.tableenhancer.warning.sortColumnIndexNotParseable";
    private static final String KEY_I18N_WARNING_NO_TABLE_FOUND = "com.tngtech.confluence.plugins.tableenhancer.warning.noTableFound";
    private static final String KEY_I18N_WARNING_FIRST_ROW_MUST_BE_HEADING_TO_SORT_TABLE = "com.tngtech.confluence.plugins.tableenhancer.warning.firstRowMustBeHeadingToSortTable";
    private static final String KEY_I18N_WARNING_SORT_COLUMN_INDEX_OUT_OF_BOUND = "com.tngtech.confluence.plugins.tableenhancer.warning.sortColumnIndexOutOfBound";
    private static final String KEY_I18N_WARNING_NUMBER_OF_FIXED_ROWS_NOT_PARSEABLE = "com.tngtech.confluence.plugins.tableenhancer.warning.numberOfFixedRowsNotParseable";
    private static final String KEY_I18N_WARNING_NUMBER_OF_FIXED_COLUMNS_NOT_PARSEABLE = "com.tngtech.confluence.plugins.tableenhancer.warning.numberOfFixedColumnsNotParseable";

    private final I18NBean i18nBean;

    public I18nService(I18NBeanFactory i18nBeanFactory) {
        this.i18nBean = i18nBeanFactory.getI18NBean();
    }

    public String sortColumnNotParsableWarning(String sortColumn) {
        return translate(KEY_I18N_WARNING_SORT_COLUMN_INDEX_NOT_PARSEABLE, sortColumn);
    }

    public String noTableFoundWarning() {
        return translate(KEY_I18N_WARNING_NO_TABLE_FOUND);
    }

    public String firstRowMustBeHeadingToSortTableWarning() {
        return translate(KEY_I18N_WARNING_FIRST_ROW_MUST_BE_HEADING_TO_SORT_TABLE);
    }

    public String sortColumnOutOfBoundWarning(int sortColumn, int size) {
        return translate(KEY_I18N_WARNING_SORT_COLUMN_INDEX_OUT_OF_BOUND, size, sortColumn);
    }

    public String numberOfFixedRowsNotParsableWarning(String numberOfFixedRows) {
        return translate(KEY_I18N_WARNING_NUMBER_OF_FIXED_ROWS_NOT_PARSEABLE, numberOfFixedRows);
    }

    public String numberOfFixedColumnsNotParsableWarning(String numberOfFixedColumns) {
        return translate(KEY_I18N_WARNING_NUMBER_OF_FIXED_COLUMNS_NOT_PARSEABLE, numberOfFixedColumns);
    }

    private String translate(String i18nKey, Object... i18nArgs) {
        return i18nBean.getText(i18nKey, i18nArgs);
    }
}
