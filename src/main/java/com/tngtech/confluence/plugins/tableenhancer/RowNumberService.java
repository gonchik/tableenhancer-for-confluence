package com.tngtech.confluence.plugins.tableenhancer;

import static com.google.common.base.Preconditions.checkArgument;

import org.jsoup.nodes.Element;

import com.tngtech.confluence.plugins.tableenhancer.Options.RowNumbers;

public class RowNumberService {

    private final ConfluenceTableService confluenceTableService;

    public RowNumberService(ConfluenceTableService confluenceTableService) {
        this.confluenceTableService = confluenceTableService;
    }

    /**
     * Add new first column to given {@code table} containing the consecutive row numbers. Therefore, the corresponding
     * cell of the current first column is cloned. Additionally, the CSS class of the {@link RowNumbers} {@code option}
     * is added to the supplied {@code table} and if necessary the <i>jQuery tablesorter</i> is switched off for the new
     * row numbers column.
     *
     * @param table which should be enhanced with row numbers
     * @param options to be checked whether table should get row numbers
     * @throws IllegalArgumentException if and only if supplied {@code table} element does not contain a table as first
     *             element
     */
    public void addRowNumbersTo(Element table, Options options) {

        if (options.hasRowNumbers()) {
            checkArgument("table".equalsIgnoreCase(table.tagName()), "Given element is not a table.");

            table.addClass(options.getRowNumbersTableClass());

            // prepend "tr" elements with a cloned first cell but numbered "td" or an empty "th", respectively
            // => clone first children due to having the same classes, coloring and so on ...
            int counter = 1;
            for (Element trElement : table.children().select("> tr")) {
                Element newElement;
                if (counter == 1 && trElement.select("> th").size() > 0) {
                    newElement = confluenceTableService.clone(trElement.child(0)).html("&nbsp;");
                    if (!options.rowNumbersColumnIsSortable()) {
                        // disable sorting for first column
                        newElement.attr("data-sorter", "false");
                    }
                } else {
                    if (options.isRowNumberBgColorAsRightNextCell()) {
                        newElement = confluenceTableService.clone(trElement.child(0));
                    } else {
                        newElement = confluenceTableService.newTd(options.getRowNumbersBgColor());
                    }
                    newElement.html(Integer.toString(counter));
                    counter++;
                }
                trElement.prependChild(newElement);
            }
        }
    }

    /**
     * @param table for which row numbers should be adjusted
     * @param options to be checked whether table should get row numbers and if they should be adjusted after sorting
     * @throws IllegalArgumentException if and only if supplied {@code table} element does not contain a table as first
     *             element
     * @throws IllegalStateException if and only if corresponding {@code options} for adding row numbers and adjusting
     *             them after sorting are set but given {@code table} has no row numbers
     */
    public void adjustRowNumbers(Element table, Options options) {

        if (!options.hasRowNumbers() || !options.rowNumbersMustBeAdjustedAfterSorting()) {
            return;
        }

        checkArgument("table".equalsIgnoreCase(table.tagName()), "Given element is not a table.");

        if (table.parent().select("> [class*=tableenhancer-rownums-]").size() < 1) {
            throw new IllegalStateException(
                    "Top level table should have row numbers but is not annotated with corresponding class. Table: "
                            + table);
        }

        int counter = 1;
        for (Element trElement : table.children().select("> tr")) {
            Element firstChild = trElement.child(0);
            if (counter != 1 || !"th".equals(firstChild.tagName())) {
                firstChild.html(Integer.toString(counter++));
            }
        }
    }
}
