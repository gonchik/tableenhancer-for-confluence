package com.tngtech.confluence.plugins.tableenhancer;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.text.NumberFormat;
import java.util.Locale;

import org.jsoup.nodes.Element;
import org.jsoup.parser.Tag;
import org.jsoup.select.Elements;

import com.google.common.annotations.VisibleForTesting;
import com.tngtech.confluence.plugins.tableenhancer.ConfluenceTableService.CellColor;

public class TotalLineService {

    private static final NumberFormat pointNumberFormat = NumberFormat.getInstance(Locale.ENGLISH);
    private static final NumberFormat commaNumberFormat = NumberFormat.getInstance(Locale.GERMAN);

    private final ConfluenceTableService confluenceTableService;

    public TotalLineService(ConfluenceTableService confluenceTableService) {
        this.confluenceTableService = confluenceTableService;
    }

    public void addTotalLineTo(Element table, Options options) {
        if (options.hasTotalLine()) {
            checkArgument("table".equalsIgnoreCase(table.tagName()), "Given element is not a table.");

            Elements trElements = table.children().select("> tr");

            // append footer as last element such that jQuery tablesorter can handle it as non-sortable row
            Element lastLineTr = table.appendElement("tfoot").appendElement("tr");

            int rowCount = trElements.size();
            int columnCount = getColumnCount(table);

            Element[][] cellElements = getCellElements(trElements, rowCount, columnCount);
            for (int col = 0; col < columnCount; col++) {
                String sumOfCol = calcSumOf(table, cellElements, col, options.useCommaAsDecimalMark());
                lastLineTr.appendChild(confluenceTableService.newTd(CellColor.GREY).html(sumOfCol));
            }
        }
    }

    private int getColumnCount(Element table) {
        int result = 0;
        for (Element element : table.children().select("> tr:first-child > *")) {
            if (element.hasAttr("rowspan")) {
                try {
                    result += Integer.valueOf(element.attr("rowspan"));
                } catch (NumberFormatException e) {
                    // ignore as html would do
                }
            } else {
                result++;
            }
        }
        return result;
    }

    private Element[][] getCellElements(Elements trElements, int rowCount, int columnCount) {
        Element[][] result = new Element[rowCount][columnCount];

        final Element dummy = new Element(Tag.valueOf("dummy"), "");
        int row = 0;
        for (Element trElement : trElements) {
            int col = 0;
            for (Element element : trElement.children()) {
                while (col < columnCount && result[row][col] != null) {
                    col++;
                }
                if (col >= columnCount) {
                    break;
                }
                result[row][col] = element;

                setDummyElementToCellsForRowspanAndColspan(result, dummy, element, row, col);

                if (result[row][col] == dummy) {
                    result[row][col] = null;
                }
                col++;
            }
            row++;
        }
        return result;
    }

    private String calcSumOf(Element table, Element[][] cellElements, int col, boolean useCommaAsDecimalMark) {
        if (col == 0 && table.attr("class").contains("tableenhancer-rownums-")) {
            return "&nbsp;";
        }

        double sum = 0;
        boolean foundNonThCell = false;
        for (Element[] cellElement : cellElements) {
            Element cell = cellElement[col];
            if (cell == null || "th".equalsIgnoreCase(cell.tagName())) {
                continue;
            }
            foundNonThCell = true;

            try {
                sum += parseNumber(cell.text(), useCommaAsDecimalMark);
            } catch (Exception e) {
                return "&nbsp;";
            }
        }
        if (!foundNonThCell) {
            return "&nbsp;";
        }
        return (useCommaAsDecimalMark ? commaNumberFormat : pointNumberFormat).format(sum);
    }

    private void setDummyElementToCellsForRowspanAndColspan(Element[][] cellElements, Element dummy,
            Element currentElement, int currentRow, int currentColum) {

        if (currentElement.hasAttr("rowspan")) {
            try {
                int rowspan = Integer.valueOf(currentElement.attr("rowspan"));
                for (int idx = 1; idx < rowspan; idx++) {
                    cellElements[currentRow + idx][currentColum] = dummy;
                }
            } catch (NumberFormatException e) {
                // ignore as html would do
            }
        }
        if (currentElement.hasAttr("colspan")) {
            try {
                int colspan = Integer.valueOf(currentElement.attr("colspan"));
                for (int idx = 1; idx < colspan; idx++) {
                    cellElements[currentRow][currentColum + idx] = currentElement;
                }
            } catch (NumberFormatException e) {
                // ignore as html would do
            }
        }
    }

    /**
     * @param str to be parsed as {@code double} (will be trimmed for normal and non-breaking spaces before parsing)
     * @param useCommaAsDecimalMark {@code true} if and only if {@code ,} (= comma) is used as decimal mark
     * @return the successfully parsed double or zero (= {@code 0}) for given empty strings such as {@code ""},
     *         {@code " "}, or {@code "\u00a0"} (= non-breaking-space)
     * @throws NullPointerException if provided {@code str} is {@code null}
     * @throws NumberFormatException if provided {@code str} could not be parsed successfully
     */
    private double parseNumber(String str, boolean useCommaAsDecimalMark) {
        checkNotNull(str, "str must not be null");

        String s = str.replaceAll("\u00a0", " ").trim();
        if (s.isEmpty()) {
            return 0;
        }

        // get string to parse without thousands separator
        if (useCommaAsDecimalMark) {
            s = s.replaceAll("\\.", "");
        } else {
            s = s.replaceAll("\\,", "");
        }

        if (useCommaAsDecimalMark) {
            s = s.replace(',', '§').replace('.', ',').replace('§', '.');
        }
        return Double.parseDouble(s);
    }
}
