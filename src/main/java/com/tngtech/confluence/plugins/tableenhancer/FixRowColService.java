package com.tngtech.confluence.plugins.tableenhancer;


import org.jsoup.nodes.Element;

public class FixRowColService {

    public void wrapWithFixTag(Element table, Options options) {
        if (options.getNumberOfFixedRows() != 0 || options.getNumberOfFixedColumns() != 0) {
            table.wrap(String.format("<div class='fixed-headers-container' data-fixed-rows='%s' data-fixed-columns='%s'></div>",
                    options.getNumberOfFixedRows(), options.getNumberOfFixedColumns()));
        }
    }
}
