package com.tngtech.confluence.plugins.tableenhancer;

import static com.google.common.base.Preconditions.checkNotNull;

import org.jsoup.nodes.Element;
import org.jsoup.parser.Tag;

public class ConfluenceTableService {

    public static enum CellColor {
        // @formatter:off
        NONE(   null,               null),
        GREY(   "highlight-grey",   "grey"),
        RED(    "highlight-red",    "red"),
        GREEN(  "highlight-green",  "green"),
        BLUE(   "highlight-blue",   "blue"),
        YELLOW( "highlight-yellow", "yellow"),

        ;
        // @formatter:on

        final String cssClass;
        final String dataHighlightColourValue;

        private CellColor(String cssClass, String dataHighlightColourValue) {
            this.cssClass = cssClass;
            this.dataHighlightColourValue = dataHighlightColourValue;
        }
    }

    /**
     * Creates a new, empty, and stand-alone {@code td}-{@link Element} for a Confluence table and the given
     * {@link CellColor} as background.
     *
     * @param cellColor the backgrond cell color for the new
     * @return the created Confluence table cell as {@link Element} (which is not connected to any other element)
     */
    public Element newTd(CellColor cellColor) {
        checkNotNull(cellColor, "cellColor must not be null");

        Element result = new Element(Tag.valueOf("td"), "").addClass("confluenceTd");
        if (cellColor != CellColor.NONE) {
            result.addClass(cellColor.cssClass).attr("data-highlight-colour", cellColor.dataHighlightColourValue);
        }
        return result;
    }

    /**
     * Creates a deep clone of the given {@code source} element, removes its {@code id} if available, and returns it
     *
     * @param source the {@link Element} to be deeply cloned
     * @return the new, cloned {@link Element}
     */
    public Element clone(Element source) {
        checkNotNull(source, "source must not be null");

        Element result = source.clone();
        result.removeAttr("id");
        return result;
    }
}
