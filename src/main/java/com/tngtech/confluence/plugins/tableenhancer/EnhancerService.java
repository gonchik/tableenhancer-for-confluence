package com.tngtech.confluence.plugins.tableenhancer;

import static com.google.common.collect.Lists.newArrayList;

import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.content.render.xhtml.definition.RichTextMacroBody;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.xhtml.api.MacroDefinition;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.google.common.collect.ImmutableMap;

public class EnhancerService {

    private final FixRowColService fixRowColService;
    private final I18nService i18nService;
    private final RowNumberService rowNumberService;
    private final SorterService sorterService;
    private final TotalLineService totalLineService;
    private final WebResourceManager webResourceManager;
    private final XhtmlContent xhtmlContent;

    public EnhancerService(FixRowColService fixRowColService, I18nService i18nService, RowNumberService rowNumberService,
            SorterService sorterService, TotalLineService totalLineService, WebResourceManager webResourceManager,
            XhtmlContent xhtmlContent) {

        this.fixRowColService = fixRowColService;
        this.i18nService = i18nService;
        this.rowNumberService = rowNumberService;
        this.sorterService = sorterService;
        this.totalLineService = totalLineService;
        this.webResourceManager = webResourceManager;
        this.xhtmlContent = xhtmlContent;
    }

    public void prepareWebResourcesFor(Options options) {
        webResourceManager.requireResourcesForContext(options.getResourcesContext());
    }

    public String createRenderedWarning(String warningMessage, ConversionContext conversionContext)
            throws MacroExecutionException {
        try {
            // @formatter:off
            MacroDefinition warningMacro = new MacroDefinition(
                    "warning",
                    new RichTextMacroBody("<p><span>" + warningMessage + "</span></p>"),
                    null,
                    ImmutableMap.<String, String> builder().put("title", "Tableenhancer").build()
                );
            // @formatter:on

            return xhtmlContent.convertMacroDefinitionToView(warningMacro, conversionContext);
        } catch (XhtmlException e) {
            throw new MacroExecutionException(e);
        }
    }

    public String enhanceTables(Options options, String bodyHtml, ConversionContext conversionContext)
            throws MacroExecutionException {

        Document doc = Jsoup.parseBodyFragment(bodyHtml);

        List<Element> tables = findTopLevelTablesRecursively(doc.body());
        if (tables.isEmpty()) {
            return createRenderedWarning(i18nService.noTableFoundWarning(), conversionContext) + "\n" + bodyHtml;
        }

        for (Element table : tables) {
            enhanceTable(table, options, conversionContext);
        }

        return doc.body().html();
    }

    private List<Element> findTopLevelTablesRecursively(Element element) {
        if ("table".equalsIgnoreCase(element.tagName())) {
            return newArrayList(element);
        }

        List<Element> result = newArrayList();
        for (Element child : element.children()) {
            result.addAll(findTopLevelTablesRecursively(child));
        }
        return result;
    }

    private void enhanceTable(Element table, Options options, ConversionContext conversionContext)
            throws MacroExecutionException {

        table.addClass("tableenhancer");

        rowNumberService.addRowNumbersTo(table, options);

        try {
            sorterService.addSortClassTo(table, options);
            if (options.isMobile()) {
                sorterService.sort(table, options);
                rowNumberService.adjustRowNumbers(table, options);
            }
        } catch (MacroExecutionException e) {
            table.before(createRenderedWarning(e.getMessage(), conversionContext));
        }

        totalLineService.addTotalLineTo(table, options);

        fixRowColService.wrapWithFixTag(table, options);
    }
}
