package com.tngtech.confluence.plugins.tableenhancer;

import static com.google.common.collect.Lists.newArrayList;

import java.util.List;
import java.util.Map;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.tngtech.confluence.plugins.tableenhancer.Options.DeviceType;
import com.tngtech.confluence.plugins.tableenhancer.Options.RowNumbers;
import com.tngtech.confluence.plugins.tableenhancer.Options.RowNumbersBgColor;
import com.tngtech.confluence.plugins.tableenhancer.Options.TotalLine;
import com.tngtech.confluence.plugins.tableenhancer.Options.TotalLine.DecimalMark;

public class OptionService {

    private static enum Parameter {

        DECIMAL_MARK("decimalMark"),
        OUTPUT_DEVICE_TYPE("output-device-type"),
        ROW_NUMBERS("rowNumbers"),
        ROW_NUMBERS_BG_COLOR("rowNumbersBgColor"),
        SORT_COLUMN("sortColumn"),
        SORT_DESCENDING("sortDescending"),
        TOTAL_LINE("totalLine"),

        NUMBER_OF_FIXED_ROWS("numberOfFixedRows"),
        NUMBER_OF_FIXED_COLUMNS("numberOfFixedColumns"),

        // Compatibility to v2.5 and before
        AUTO_NUMBER("autoNumber"),

        ;

        private final String paramPropertyValue;

        private Parameter(String paramPropertyValue) {
            this.paramPropertyValue = paramPropertyValue;
        }

        public String from(Map<String, String> params) {
            return params.get(paramPropertyValue);
        }

        public String from(ConversionContext context) {
            return context.getPropertyAsString(paramPropertyValue);
        }
    }

    private final I18nService i18nService;

    public OptionService(I18nService i18nService) {
        this.i18nService = i18nService;
    }

    public Options parse(Map<String, String> parameters, ConversionContext conversionContext) {
        List<String> warningMessages = newArrayList();

        Options.DeviceType deviceTypeOption = getDeviceTypeOptionFrom(conversionContext);
        Options.RowNumbers rowNumbersOption = getRowNumbersOptionFrom(parameters);
        Options.RowNumbersBgColor rowNumbersBgColorOption = getRowNumbersBgColorOptionFrom(parameters);
        Options.Sort sortOption = getSortOptionFrom(parameters, warningMessages);
        Options.TotalLine totalLineOption = getTotalLineOptionFrom(parameters);
        int numberOfFixedRows = getNumberOfFixedRowsFrom(parameters, warningMessages);
        int numberOfFixedColumns = getNumberOfFixedColumnsFrom(parameters, warningMessages);

        return new Options(deviceTypeOption, rowNumbersOption, rowNumbersBgColorOption, sortOption, totalLineOption,
                numberOfFixedRows, numberOfFixedColumns, warningMessages);
    }

    /** @return matching {@link DeviceType} (never {@code null} */
    private Options.DeviceType getDeviceTypeOptionFrom(ConversionContext conversionContext) {
        String paramValue = Parameter.OUTPUT_DEVICE_TYPE.from(conversionContext);
        for (Options.DeviceType deviceTypeOption : Options.DeviceType.values()) {
            if (deviceTypeOption.propertyValue.equals(paramValue)) {
                return deviceTypeOption;
            }
        }
        return Options.DeviceType.DESKTOP;
    }

    /** @return matching {@link RowNumbers} (never {@code null} */
    private Options.RowNumbers getRowNumbersOptionFrom(Map<String, String> parameters) {
        String paramValue = OptionService.Parameter.ROW_NUMBERS.from(parameters);
        for (Options.RowNumbers rowNumbersOption : Options.RowNumbers.values()) {
            if (rowNumbersOption.propertyValue.equals(paramValue)) {
                return rowNumbersOption;
            }
        }

        paramValue = OptionService.Parameter.AUTO_NUMBER.from(parameters);
        for (Options.RowNumbers rowNumbersOption : Options.RowNumbers.values()) {
            if (rowNumbersOption.propertyValue.equals(paramValue)) {
                return rowNumbersOption;
            }
        }

        return Options.RowNumbers.NONE;
    }

    /** @return matching {@link RowNumbersBgColor} (never {@code null} */
    private Options.RowNumbersBgColor getRowNumbersBgColorOptionFrom(Map<String, String> parameters) {
        String paramValue = OptionService.Parameter.ROW_NUMBERS_BG_COLOR.from(parameters);
        for (Options.RowNumbersBgColor rowNumbersBgColorOption : Options.RowNumbersBgColor.values()) {
            if (rowNumbersBgColorOption.propertyValue.equals(paramValue)) {
                return rowNumbersBgColorOption;
            }
        }
        return Options.RowNumbersBgColor.GREY;
    }

    private Options.Sort getSortOptionFrom(Map<String, String> parameters, List<String> warningMessages) {
    	try {
    		String sortColumnValue = Parameter.SORT_COLUMN.from(parameters);
    		String sortDescendingValue = Parameter.SORT_DESCENDING.from(parameters);

    		if (sortColumnValue != null) {
    			int sortColumn = Integer.parseInt(sortColumnValue);
    			Options.Sort.Order sortOrder = ("true".equals(sortDescendingValue)) ? Options.Sort.Order.DESCENDING
    					: Options.Sort.Order.ASCENDING;
    			return new Options.Sort(sortColumn - 1, sortOrder);
    		}
    		return Options.Sort.NONE;
    	} catch (NumberFormatException e) {
    		warningMessages.add(i18nService.sortColumnNotParsableWarning(Parameter.SORT_COLUMN.from(parameters)));
    		return Options.Sort.NONE;
    	}
    }

    /** @return matching {@link TotalLine} (never {@code null} */
    private Options.TotalLine getTotalLineOptionFrom(Map<String, String> parameters) {
        if ("true".equals(Parameter.TOTAL_LINE.from(parameters))) {
            String paramValue = Parameter.DECIMAL_MARK.from(parameters);
            for (DecimalMark decimalMark : DecimalMark.values()) {
                if (decimalMark.propertyValue.equals(paramValue)) {
                    return new Options.TotalLine(true, decimalMark);
                }
            }
            return new Options.TotalLine(true, DecimalMark.POINT);
        }
        return Options.TotalLine.NONE;
    }

    private int getNumberOfFixedRowsFrom(Map<String, String> parameters, List<String> warningMessages) {
    	try {
	    	String numberOfFixedRowsValue = Parameter.NUMBER_OF_FIXED_ROWS.from(parameters);
	    	if (numberOfFixedRowsValue != null) {
	    		return Integer.parseInt(numberOfFixedRowsValue);
	    	}
	    	return 0;
    	} catch (NumberFormatException e) {
    		warningMessages.add(i18nService.numberOfFixedRowsNotParsableWarning(Parameter.NUMBER_OF_FIXED_ROWS.from(parameters)));
    		return 0;
    	}
    }

    private int getNumberOfFixedColumnsFrom(Map<String, String> parameters, List<String> warningMessages) {
    	try {
    		String numberOfFixedColumnsValue = Parameter.NUMBER_OF_FIXED_COLUMNS.from(parameters);
    		if (numberOfFixedColumnsValue != null) {
    			return Integer.parseInt(numberOfFixedColumnsValue);
    		}
    		return 0;
    	} catch (NumberFormatException e) {
    		warningMessages.add(i18nService.numberOfFixedColumnsNotParsableWarning(Parameter.NUMBER_OF_FIXED_COLUMNS.from(parameters)));
    		return 0;
    	}
    }
}
