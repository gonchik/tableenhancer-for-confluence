package com.tngtech.confluence.plugins.tableenhancer;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.Collections;
import java.util.Comparator;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.atlassian.confluence.macro.MacroExecutionException;
import com.tngtech.confluence.plugins.tableenhancer.Options.RowNumbers;

public class SorterService {

    private final I18nService i18nService;

    public SorterService(I18nService i18nService) {
        this.i18nService = i18nService;
    }

    /**
     * Add new first column to given {@code table} containing the consecutive row numbers. Therefore, the corresponding
     * cell of the current first column is cloned. Additionally, the CSS class of the {@link RowNumbers} {@code option}
     * is added to the supplied {@code table} and if necessary the <i>jQuery tablesorter</i> is switched off for the new
     * row numbers column.
     *
     * @param table which should be enhanced with row numbers
     * @param options to be checked whether table should get row numbers
     * @throws IllegalArgumentException if and only if supplied {@code table} element does not contain a table as first
     *             element
     * @throws MacroExecutionException if and only if supplied {@code table} cannot be sorted due to any errors
     */
    public void addSortClassTo(Element table, Options options) throws MacroExecutionException {
        if (options.sortTable()) {
            checkTableToBeSorted(table, options);
            table.children().select("> tr > th").get(options.sortColumnIndex()).addClass(options.getSortOrderThClass());
        }
    }

    /**
     * Sorts the given table {@link Element} according to the supplied {@link Options}.
     *
     * @param table to be sorted
     * @param options to determine sorting column and whether ascending or descending
     * @throws IllegalArgumentException if and only if supplied {@code table} element does not contain a table as first
     *             element
     * @throws MacroExecutionException if and only if supplied {@code table} cannot be sorted due to any errors
     */
    public void sort(Element table, final Options options) throws MacroExecutionException {

        if (options.sortTable()) {
            checkTableToBeSorted(table, options);

            Elements trElements = table.children().select("> tr");
            trElements.remove(0); // remove first row which is always a heading row (see check) => should not be sorted

            Collections.sort(trElements, new Comparator<Element>() {
                private final int sortColumnIndex = options.sortColumnIndex();
                private final boolean sortDescending = options.sortDescending();

                @Override
                public int compare(Element o1, Element o2) {
                    String childValue1 = o1.child(sortColumnIndex).html();
                    String childValue2 = o2.child(sortColumnIndex).html();
                    if (sortDescending) {
                        return childValue2.compareTo(childValue1); // String compare
                    }
                    return childValue1.compareTo(childValue2); // String compare
                }
            });

            Element tbody = table.child(0);
            for (Element trElement : trElements) {
                trElement.remove();
                tbody.appendChild(trElement);
            }
        }
    }

    private void checkTableToBeSorted(Element table, Options options) throws MacroExecutionException {
        checkArgument("table".equalsIgnoreCase(table.tagName()), "Given element is not a table.");

        int columnCount = table.children().select("> tr:first-child > th").size();
        if (columnCount <= 0) {
            throw new MacroExecutionException(i18nService.firstRowMustBeHeadingToSortTableWarning());

        } else if (!options.isSortColumnIndexValidFor(columnCount)) {
            throw new MacroExecutionException(i18nService.sortColumnOutOfBoundWarning(options.sortColumnUserIndex(),
                    (options.hasRowNumbers()) ? columnCount - 1 : columnCount));
        }
    }
}
