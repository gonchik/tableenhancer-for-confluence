package com.tngtech.confluence.plugins.tableenhancer;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import static java.util.Collections.unmodifiableList;

import java.util.List;

import com.google.common.annotations.VisibleForTesting;
import com.tngtech.confluence.plugins.tableenhancer.ConfluenceTableService.CellColor;
import com.tngtech.confluence.plugins.tableenhancer.Options.TotalLine.DecimalMark;

public class Options {

    public static enum DeviceType {
        // @formatter:off
        DESKTOP("desktop", "com.tngtech.confluence.plugins.tableenhancer"),
        MOBILE( "mobile",  "com.tngtech.confluence.plugins.tableenhancer.mobile")

        ;
        // @formatter:on

        public final String propertyValue;

        private final String resourcesContext;

        private DeviceType(String propertyValue, String resourcesContext) {
            this.propertyValue = propertyValue;
            this.resourcesContext = resourcesContext;
        }
    }

    public static enum RowNumbers {

        // @formatter:off
        NONE(                    "",                       false, false,  ""),
        ONCE_BEFORE_SORTING(     "onceBeforeSorting",      true,  true,  "tableenhancer-rownums-onceBeforeSorting"),
        INDEPENDENT_FROM_SORTING("independentFromSorting", true,  false, "tableenhancer-rownums-independentFromSorting"),
        ONCE_AFTER_SORTING(      "onceAfterSorting",       true,  true,  "tableenhancer-rownums-onceAfterSorting"),

        // Compatibility to v1.0
        TRUE(                    "true",                   true,  true,  "tableenhancer-rownums-onceBeforeSorting")
        ;
        // @formatter:on

        public final String propertyValue;

        private final boolean hasRowNums;
        private final boolean columnIsSortable;
        private final String tableClass;

        private RowNumbers(String propertyValue, boolean hasRowNums, boolean columnIsSortable, String tableClass) {
            this.propertyValue = propertyValue;
            this.hasRowNums = hasRowNums;
            this.columnIsSortable = columnIsSortable;
            this.tableClass = tableClass;
        }
    }

    public static enum RowNumbersBgColor {

        // @formatter:off
        GREY(               "grey",             CellColor.GREY),
        RED(                "red",              CellColor.RED),
        GREEN(              "green",            CellColor.GREEN),
        BLUE(               "blue",             CellColor.BLUE),
        YELLOW(             "yellow",           CellColor.YELLOW),
        TRANSPARENT(        "transparent",      CellColor.NONE),
        AS_RIGHT_NEXT_CELL( "asRightNextCell",  null),

        ;
        // @formatter:on

        public final String propertyValue;

        private final CellColor cellColor;

        private RowNumbersBgColor(String propertyValue, CellColor cellColor) {
            this.propertyValue = propertyValue;
            this.cellColor = cellColor;
        }
    }

    public static class Sort {

        public static enum Order {
            ASCENDING("tableenhancer-sort-asc"),
            DESCENDING("tableenhancer-sort-desc");

            private final String thClass;

            private Order(String thClass) {
                this.thClass = thClass;
            }
        }

        public static final Sort NONE = new Sort(null, null);

        private final Integer columnIndex;
        private final Sort.Order order;

        public Sort(Integer columnIndex, Sort.Order sortOrder) {
            if (columnIndex != null) {
                checkArgument(sortOrder != null, "sortOrder must not be null if columnIndex is specified");
            }
            this.columnIndex = columnIndex;
            this.order = sortOrder;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((columnIndex == null) ? 0 : columnIndex.hashCode());
            result = prime * result + ((order == null) ? 0 : order.hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            Sort other = (Sort) obj;
            if (columnIndex == null) {
                if (other.columnIndex != null) {
                    return false;
                }
            } else if (!columnIndex.equals(other.columnIndex)) {
                return false;
            }
            return order == other.order;
        }

        @Override
        public String toString() {
            return "Sort [columnIndex=" + columnIndex + ", order=" + order + "]";
        }
    }

    public static class TotalLine {

        public static enum DecimalMark {
            POINT(". (point)"),
            COMMA(", (comma)");

            public final String propertyValue;

            private DecimalMark(String propertyValue) {
                this.propertyValue = propertyValue;
            }
        }

        public static final TotalLine NONE = new TotalLine(false, DecimalMark.POINT);

        private final boolean hasTotalLine;
        private final DecimalMark decimalMark;

        public TotalLine(boolean hasTotalLine, DecimalMark decimalMark) {
            this.hasTotalLine = hasTotalLine;
            this.decimalMark = decimalMark;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((decimalMark == null) ? 0 : decimalMark.hashCode());
            result = prime * result + (hasTotalLine ? 1231 : 1237);
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            TotalLine other = (TotalLine) obj;
            if (decimalMark != other.decimalMark) {
                return false;
            }
            return hasTotalLine == other.hasTotalLine;
        }

        @Override
        public String toString() {
            return "TotalLine [hasTotalLine=" + hasTotalLine + ", decimalMark=" + decimalMark + "]";
        }
    }

    private final DeviceType deviceType;
    private final RowNumbers rowNumbers;
    private final RowNumbersBgColor rowNumbersBgColor;
    private final Sort sort;
    private final TotalLine totalLine;
    private final int numberOfFixedRows;
    private final int numberOfFixedColumns;
    private final List<String> warningMessages;

    public Options(DeviceType deviceType, RowNumbers rowNumbers, RowNumbersBgColor rowNumbersBgColor, Sort sort,
            TotalLine totalLine, int numberOfFixedRows, Integer numberOfFixedColumns, List<String> warningMessages) {

        this.rowNumbers = checkNotNull(rowNumbers, "rowNumbers option must not be null");
        this.rowNumbersBgColor = checkNotNull(rowNumbersBgColor, "rowNumbersBgColor option must not be null");
        this.deviceType = checkNotNull(deviceType, "deviceType option must not be null");
        this.sort = checkNotNull(sort, "sort option must not be null");
        this.totalLine = checkNotNull(totalLine, "totalLine option must not be null");
        this.numberOfFixedRows = checkNotNull(numberOfFixedRows, "number of fixed rows option must not be null");
        this.numberOfFixedColumns = checkNotNull(numberOfFixedColumns, "number of fixed columns option must not be null");
        this.warningMessages = unmodifiableList(checkNotNull(warningMessages, "warningMessages must not be null"));
    }

    // Device Type
    public boolean isMobile() {
        return deviceType == DeviceType.MOBILE;
    }

    public String getResourcesContext() {
        return deviceType.resourcesContext;
    }

    // RowNumbers
    public boolean hasRowNumbers() {
        return rowNumbers.hasRowNums;
    }

    public boolean rowNumbersColumnIsSortable() {
        return rowNumbers.columnIsSortable;
    }

    public String getRowNumbersTableClass() {
        return rowNumbers.tableClass;
    }

    public boolean rowNumbersMustBeAdjustedAfterSorting() {
        return rowNumbers == RowNumbers.INDEPENDENT_FROM_SORTING || rowNumbers == RowNumbers.ONCE_AFTER_SORTING;
    }

    // RowNumbersBgColor
    public CellColor getRowNumbersBgColor() {
        return rowNumbersBgColor.cellColor;
    }

    public boolean isRowNumberBgColorAsRightNextCell() {
        return rowNumbersBgColor == RowNumbersBgColor.AS_RIGHT_NEXT_CELL;
    }

    // Sort
    public boolean sortTable() {
        return sort.columnIndex != null;
    }

    public int sortColumnIndex() {
        checkState(sortTable(), "column index not available if table should not be sorted");
        return (hasRowNumbers()) ? sort.columnIndex + 1 : sort.columnIndex;
    }

    public int sortColumnUserIndex() {
        checkState(sortTable(), "user column index not available if table should not be sorted");
        return sort.columnIndex + 1;
    }

    public boolean isSortColumnIndexValidFor(int tableSize) {
        checkState(sortTable(), "column index check not available if table should not be sorted");
        return 0 <= sortColumnIndex() && sortColumnIndex() < tableSize;
    }

    public boolean sortDescending() {
        checkState(sortTable(), "column sort order not available if table should not be sorted");
        return sort.order == Sort.Order.DESCENDING;
    }

    public String getSortOrderThClass() {
        return (sort.order == null) ? "" : sort.order.thClass;
    }

    // TotalLine
    public boolean hasTotalLine() {
        return totalLine.hasTotalLine;
    }

    public boolean useCommaAsDecimalMark() {
        return totalLine.decimalMark == DecimalMark.COMMA;
    }

    // FixedHeaders
    public int getNumberOfFixedRows() {
    	return numberOfFixedRows;
    }

    public int getNumberOfFixedColumns() {
    	return numberOfFixedColumns;
    }

    // Warnings
    public List<String> getWarningMessages() {
        return warningMessages;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((deviceType == null) ? 0 : deviceType.hashCode());
        result = prime * result + ((rowNumbers == null) ? 0 : rowNumbers.hashCode());
        result = prime * result + ((rowNumbersBgColor == null) ? 0 : rowNumbersBgColor.hashCode());
        result = prime * result + ((sort == null) ? 0 : sort.hashCode());
        result = prime * result + ((totalLine == null) ? 0 : totalLine.hashCode());
        result = prime * result + ((warningMessages == null) ? 0 : warningMessages.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Options other = (Options) obj;
        if (deviceType != other.deviceType) {
            return false;
        }
        if (rowNumbers != other.rowNumbers) {
            return false;
        }
        if (rowNumbersBgColor != other.rowNumbersBgColor) {
            return false;
        }
        if (sort == null) {
            if (other.sort != null) {
                return false;
            }
        } else if (!sort.equals(other.sort)) {
            return false;
        }
        if (totalLine == null) {
            if (other.totalLine != null) {
                return false;
            }
        } else if (!totalLine.equals(other.totalLine)) {
            return false;
        }
        if (warningMessages == null) {
            if (other.warningMessages != null) {
                return false;
            }
        } else if (!warningMessages.equals(other.warningMessages)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Options [deviceType=" + deviceType + ", rowNumbers=" + rowNumbers + ", rowNumbersBgColor="
                + rowNumbersBgColor + ", sort=" + sort + ", totalLine=" + totalLine + ", warningMessages="
                + warningMessages + "]";
    }
}
