/*global AJS:false tinymce:false */
(function () {

var init = function () {
    var inactivityTimeoutID = 0,
        $ = AJS.$,
        iframe = $('#wysiwygTextarea_ifr');

    /*
     * Handle events, which require updating
     */
    // Save Handler
    var saveHandler = function () {
        try {
            $("#wysiwygTextarea_ifr").contents().find("body").off("keyup paste");
            removeFixedHeadersHTMLFromIframe();
        } catch (e) {
            AJS.logError("table enhancer: error in save handler", e);
        }
    };
    Confluence.Editor.addSaveHandler(saveHandler);

    // the quick edit bar removes all save/cancel handlers on init, so we have to bind to a later event to re-add it
    AJS.bind("rte-quick-edit-ready", function () {
        Confluence.Editor.addSaveHandler(saveHandler);
    });

    // Preview Button Handler
    $('#rte-button-preview').mousedown(function (eventObject) {
        // need to be ahead of other event handlers for this button, so bind to mousedown
        if (eventObject.which === 1) { // left mouse button
            try {
                removeFixedHeadersHTMLFromIframe();
                $('#rte-button-preview').click();
            } catch (e) {
                AJS.logError("table enhancer: error in preview handler", e);
            }
        }
    });

    // Edit Button Handler (when returning from preview mode)
    $('#rte-button-edit').click(setupMacro);

    // Initial Setup and Reload Handler
    AJS.Rte.getEditor().setContent = appendToFunction(AJS.Rte.getEditor().setContent,
        function () {
            setupMacroAfterDelay();
        });


    // New Macro via AutoComplete Handler and Macro Configuration Handler for the Macro Browser (changes or new macro)
    tinymce.confluence.MacroUtils.insertMacro = appendToFunction(tinymce.confluence.MacroUtils.insertMacro, setupMacroAfterDelay);

    // Table Insertion Handler
    AJS.Rte.getEditor().plugins.table.insertTable = appendToFunction(AJS.Rte.getEditor().plugins.table.insertTable, setupMacroAfterDelay)


    /*
     * Function definitions
     */

    // Full setup function

    function setupMacro() {
        try {
            $("#wysiwygTextarea_ifr").contents().find("body").off("keyup paste", iframeEditingHandler);
            $("#wysiwygTextarea_ifr").contents().find("body").off("paste", function () {
                setTimeout(checkIfSetupNecessary, 100);
            });
            $("#wysiwygTextarea_ifr").contents().find("body").off("keyup paste", restartInactivityCountDown);
            $("#rte-toolbar").off("click", iframeEditingHandler);
            $("#wysiwygTextarea_ifr").contents().find(".fixed-headers-container").off("click", ".clone", onClickOnClone);

            removeFixedHeadersHTMLFromIframe();
            initializeTableContainers();
            initializeTableSetup();

            iframe.trigger("refreshClones.tableenhancer");

            restartInactivityCountDown();

            $("#wysiwygTextarea_ifr").contents().find("body").on("keyup paste", restartInactivityCountDown);
            $("#wysiwygTextarea_ifr").contents().find("body").on("keyup paste", iframeEditingHandler);
            $("#wysiwygTextarea_ifr").contents().find("body").on("paste", function () {
                setTimeout(checkIfSetupNecessary, 100);
            });
            $("#rte-toolbar").on("click", iframeEditingHandler);

            $("#wysiwygTextarea_ifr").contents().find(".fixed-headers-container").on("click", "div.clone", onClickOnClone);
        } catch (e) {
            AJS.logError("table enhancer: error in setup", e);
        }
    }

    function setupMacroAfterDelay() {
        setTimeout(setupMacro, 500);
    }

    function restartInactivityCountDown() {
        window.clearTimeout(inactivityTimeoutID);
        inactivityTimeoutID = window.setTimeout(checkIfSetupNecessary, 5000);
    }

    // Event Handlers

    function onClickOnClone(eventObject) {
        var closestCellElement = $(eventObject.target).closest("td, th"),
            closestRowElement = $(eventObject.target).closest("tr"),
            triggeringClone = $(eventObject.target).closest(".clone"),
            activeContainer = $(eventObject.target).closest(".fixed-headers-container"),
            index_in_row = 0,
            index_of_row = 0,
            type_of_row_container = "TBODY";

        if (closestCellElement.length > 0) {
            index_in_row = closestCellElement.index();
            index_of_row = closestRowElement.index();
            type_of_row_container = closestRowElement.parent("tbody, thead").prop("tagName");
        } else if (closestRowElement.length > 0) {
            index_of_row = closestRowElement.index();
            type_of_row_container = closestRowElement.parent("tbody, thead").prop("tagName");
        } else {
            // leave defaults
        }
        var startOffset = AJS.Rte.getEditor().selection.getRng().startOffset || 0;

        var targetInOriginalTable = $(activeContainer).find(type_of_row_container + "> tr:eq(" + index_of_row + ")> :eq(" + index_in_row + ")");

        AJS.Rte.getEditor().selection.select(targetInOriginalTable.contents()[targetInOriginalTable.contents().length - 1]);
        AJS.Rte.getEditor().selection.collapse(false);

        if (triggeringClone.hasClass("vert_clone")) {
            $("#wysiwygTextarea_ifr")[0].contentWindow.scrollTo(iframe.scrollLeft(), activeContainer.offset().top - 10);
        } else if (triggeringClone.hasClass("hori_clone")) {
            $("#wysiwygTextarea_ifr")[0].contentWindow.scrollTo(activeContainer.offset().left - 10, iframe.scrollTop());
        } else {
            $("#wysiwygTextarea_ifr")[0].contentWindow.scrollTo(activeContainer.offset().left - 10, activeContainer.offset().top - 10);
        }
    }

    function iframeEditingHandler() {
        var currentRange = AJS.Rte.getEditor().selection.getRng();

        checkForAndRemoveInsertedParagraphs();

        if (($(currentRange.startContainer).closest(".fixed-headers-container").length > 0) ||
            ($(currentRange.endContainer).closest(".fixed-headers-container").length > 0)) {
            // so change happened within an active macro

            iframe.trigger("refreshClones.tableenhancer");
        }
    }

    // Remover functions

    function removeFixedHeadersHTMLFromIframe() {
        var iframeContent = $("#wysiwygTextarea_ifr").contents();
        $(".clone", iframeContent).remove();
        $(".tableenhancer-fixed-row", iframeContent).removeClass("tableenhancer-fixed-row");
        $(".tableenhancer-fixed-column", iframeContent).removeClass("tableenhancer-fixed-column");
        $(".fixed-headers-bottom-anchor", iframeContent).remove();
        $(".fixed-headers-container", iframeContent).each(function (index, containerElement) {
            $(containerElement, iframeContent).children("p:first-child, p:last-child").children("br:only-child").parent().remove();
            if ($(containerElement, iframeContent).prev().length > 0) {
                $(containerElement, iframeContent).contents().insertAfter($(containerElement, iframeContent).prev());
            } else {
                $(containerElement, iframeContent).contents().appendTo($(containerElement, iframeContent).parent());
            }
            $(containerElement, iframeContent).remove();
        });
    }

    function checkForAndRemoveInsertedParagraphs() {
        //Dont remove the auto-cursor target as this gets immediately inserted again and so looks strange
        //additional shifting for the clones is handled
        $("#wysiwygTextarea_ifr").contents().find(".fixed-headers-container").children("p:not(.auto-cursor-target)").remove();
    }

    // Scan iframe content for unhandled tables/non-initialized tableenhancer macros

    function checkIfSetupNecessary() {
        var iframeContent = $("#wysiwygTextarea_ifr").contents();
        $("table[data-macro-name=tableenhancer]", iframeContent).each(function (index, tableElement) {
            if ($(tableElement, iframeContent).find("table:not(.clone):not(.confluenceTable .confluencTable):not(.fixed-headers-container table)").length > 0) {
                setupMacro();
            }
        });
    }

    // Container Initialization

    function initializeTableContainers() {
        $("#wysiwygTextarea_ifr").contents().find("table[data-macro-name=tableenhancer]").each(
            function (index, element) {
                var settingsObject = getMacroSettings(element);
                configureTableContainersForMacro(
                    element,
                    settingsObject.numberOfFixedRows,
                    (settingsObject.isRowNumberingActivated ? settingsObject.numberOfFixedColumns - 1 : settingsObject.numberOfFixedColumns)
                ); // if row numbering is active, the numbering column doesn't appear in the editor, so reduce the number of fixed columns in the editor by 1
            }
        );
    }

    function getMacroSettings(macroElement) {
        var parameters = $(macroElement).data("macro-parameters"),
            numberOfFixedRows = 0, 				// default
            numberOfFixedColumns = 0, 			// default
            isRowNumberingActivated = false; 	// default

        if (parameters !== undefined) {
            var paramArray = parameters.split(/[\|=]/g);
            numberOfFixedRows = ( (paramArray.indexOf("numberOfFixedRows") >= 0) ? paramArray[paramArray.indexOf("numberOfFixedRows") + 1] : 0);
            numberOfFixedColumns = ( (paramArray.indexOf("numberOfFixedColumns") >= 0) ? paramArray[paramArray.indexOf("numberOfFixedColumns") + 1] : 0);
            isRowNumberingActivated = (paramArray.indexOf("rowNumbers") >= 0);
        }

        return {
            numberOfFixedRows: numberOfFixedRows,
            numberOfFixedColumns: numberOfFixedColumns,
            isRowNumberingActivated: isRowNumberingActivated
        };
    }

    function configureTableContainersForMacro(macroElement, numOfFixedRows, numOfFixedColumns) {
        $(macroElement).find("tbody > tr > td.wysiwyg-macro-body table.confluenceTable:not(.confluenceTable .confluenceTable)").each( // iterate over all top-level tables within the macro
            function (index, tableElement) {
                $(tableElement).wrap('<div class="fixed-headers-container" data-fixed-rows=' + numOfFixedRows + ' data-fixed-columns=' + numOfFixedColumns + '></div>');
            }
        );
    }

    // Table Setup

    function initializeTableSetup() {
        $(window).trigger("activateFixedHeaders.tableenhancer", ["editor"]);
    }

    // Helper Functions

    function appendToFunction(targetFunction, functionToAppend) {
        if (typeof targetFunction === "function") {
            return function () {
                var targetOutput = targetFunction.apply(this, arguments);
                functionToAppend();
                return targetOutput;
            };
        } else {
            AJS.log("tableenhancer warning: appendToFunction expected a function, but got", targetFunction);
        }
    }
};

AJS.bind("init.rte", function () {
    try {
        init();
    } catch (e) {
        AJS.logError("table enhancer: error in initialization", e);
    }
});

}());
