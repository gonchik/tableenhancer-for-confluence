AJS.toInit(function () {
    var $  = AJS.$;

    $('table.tableenhancer').bind('tablesorter-initialized', function (e, table) {

        var $table = $(table);
        if (!$table.hasClass('tableenhancer')) { // TODO ASc why will this event
            // be call for all tables?
            return;
        }
        var sortBy = [];

        var ths = $table.find('th');
        for (var i = 0; i < ths.length; ++i) {
            var th = $(ths[i]);
            if (th.hasClass('tableenhancer-sort-asc')) {
                sortBy.push([ i, 0 ]);
                th.removeClass('tableenhancer-sort-asc');
            } else if (th.hasClass('tableenhancer-sort-desc')) {
                sortBy.push([ i, 1 ]);
                th.removeClass('tableenhancer-sort-desc');
            }
        }

        detectParsersForColumns(table);

        $table.trigger('update'); // initialize caches for sorting etc.

        if ($table.hasClass('tableenhancer-rownums-independentFromSorting')) {
            $table.bind('sortEnd', function (e, table) {
                adjustRowNumbers(table);
            });
        } else if ($table.hasClass('tableenhancer-rownums-onceAfterSorting')) {
            $table.one('sortEnd', function (e, table) {
                adjustRowNumbers(table);
            });
        }

        if (sortBy.length > 0) {
            $table.trigger('sorton', [ sortBy ]);
        }

        $table.trigger('tableenhancer-rownums-completed');
    });

    function adjustRowNumbers(table) {
        $(table).find('> tbody > tr').each(function (idx, tr) {
            $(tr).children(':first').html(idx + 1);
        });
    }

    /*
     * Functions adapted from the tablesorter Plugin for jQuery for better detection of the appropriate parser for sorting
     */

    function detectParsersForColumns(table) {
        var c = table.config,
            tb = c.$tbodies,
            rows, l, i, parser_id;
        if (tb.length === 0) {
            return '';
        }
        rows = tb[0].rows;
        if (rows[0]) {
            l = rows[0].cells.length;  // the number of columns
            for (i = 0; i < l; i++) {
                parser_id = detectParserForColumn(table, rows, i);
                c.$headers.filter('[data-column="' + i + '"]:last').data('sorter', parser_id)
            }
        }
        // $(table).trigger('update')
    }

    function detectParserForColumn(table, rows, columnIndex) {
        var cur, current_id, hit_break_statement,
            config = $(table).data('tablesorter'),
            parsers = $.tablesorter.parsers,
            i = parsers.length,
            node, nodeValue, rowIndex = 0, numRows = rows.length;

        current_id = config.parsers[columnIndex].id;
        if (current_id === 'issue-key' || current_id === 'text') {
            return current_id;  // if it is 'issue-key', then must have been set externally; 'text' is the least restrictive, so we can not improve on it
        }
        while (--i > 0) { // do not need to check parsers[0] (the 'text' parser), since it is always true
            cur = parsers[i];
            hit_break_statement = false;
            for (rowIndex = 0; rowIndex < numRows; rowIndex++) {
                node = rows[rowIndex].cells[columnIndex];
                nodeValue = getElementText(table, node, columnIndex);
                if (nodeValue !== '') {
                    if (!(cur && cur.is && cur.is(nodeValue, table, node))) { // Note: There's a NOT at the start of the expression
                        hit_break_statement = true;
                        break; // so there is a non-empty cell not fitting to the current parser, so try the next one
                    }
                }
            }
            if (!hit_break_statement) {
                return cur.id; // so all cells in this columns matched the parser
            }
        }
        return 'text';
    }

    function getElementText(table, node, columnIndex) {
        if (!node) { return ""; }
        var c = table.config,
            t = c.textExtraction, text = "";
        if (t === "simple") {
            if (c.supportsTextContent) {
                text = node.textContent; // newer browsers support this
            } else {
                text = $(node).text();
            }
        } else {
            if (typeof t === "function") {
                text = t(node, table, columnIndex);
            } else if (typeof t === "object" && t.hasOwnProperty(columnIndex)) {
                text = t[columnIndex](node, table, columnIndex);
            } else {
                text = c.supportsTextContent ? node.textContent : $(node).text();
            }
        }
        return $.trim(text);
    }
});