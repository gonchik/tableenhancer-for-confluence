/*global AJS:false Confluence:false */
AJS.toInit(function() {

var activateFixedHeaders = function (event, environment) {
    /*
     * PART 1: General configuration and variable declaration
     */

    // - COVER_BEFORE_SCROLL_OUT: between 0 and 1, minimum of the screen covered by the table
    // before the table header scrolls out
    // - MAX_NUMBER_OF_KEPT_ROWS: max number of rows below the header when the header scrolls out of view
    var COVER_BEFORE_SCROLL_OUT = 0.6,
        MAX_NUMBER_OF_KEPT_ROWS = 20,
        winObj, docObj, browser,
        number_of_tables_with_macro,
        table_top_correction, div_top_correction, table_left_correction, div_left_correction,
        height_of_bottom_x_rows = [],
        number_of_bottom_rows_kept = [],
        scroll_top, scroll_bottom, element_top, element_bottom, element_height, anchor_bottom,
        table_in_macro, fixed_row_element, fixed_column_element, originating_div,
        current_table_number,
        scroll_left, element_left, element_right, element_width,
        anchors_for_refresh, iframe, $ = AJS.$;;


    /*
     * PART 2: Environment specific setup
     */

    if (environment === "editor") {
        var iframe = $("#wysiwygTextarea_ifr");
        docObj = iframe.contents();
        winObj = iframe[0].contentWindow ? iframe[0].contentWindow : iframe[0].contentDocument.defaultView;
    } else {
        docObj = document;
        winObj = window;
    }


    /*
     * PART 3: Initialize the macro: Setup and Event Handler Registration
     */

    number_of_tables_with_macro = $(".fixed-headers-container", docObj).length;

    // wrap the table within the macro and add an anchor to mark the end of the macro
    $(".fixed-headers-container table:not(.clone):not(.confluenceTable .confluenceTable):not(.wysiwyg-macro)", docObj)
        .after('<div class="fixed-headers-bottom-anchor synchrony-exclude"></div>');

    // label the wrappers and anchors, in case the macro appears several times on a page
    $(".fixed-headers-container", docObj).addClass(function (index) {
        return(index.toString());
    });
    $(".fixed-headers-bottom-anchor", docObj).addClass(function (index) {
        return(index.toString());
    });

    if ($(".fixed-headers-bottom-anchor", docObj).length > 0) {
        anchors_for_refresh = $("div.fixed-headers-bottom-anchor", docObj);
    }


    $(".fixed-headers-container", docObj).each(function (index, element) {
        var table = $(element).find("table:not(.clone):not(.confluenceTable .confluenceTable)");
        if (table.length > 1) {
            Confluence.Editor.addErrorMessage(
                'com_tngtech_confluence_plugins_tableenhancer_error_nested-table', // may not contain "." !!!
                'Fixing rows/columns is only supported for top level tables, nested tables may not display correctly!',
                false // showInAllModes
            );
        }

        //Checks if the table contains cells spanning more rows/columns as confluence disables sorting for this and we also need to do this
        var hasRowOrColSpan = false;
        $(table.find("[colspan]")).each(function (index, element){
            if($(element).attr("colspan") > 1){
                hasRowOrColSpan = true;
            }
        });
        $(table.find("[rowspan]")).each(function (index, element){
            if($(element).attr("rowspan") > 1){
                hasRowOrColSpan = true;
            }
        });

        if (hasRowOrColSpan || environment === "editor") { // JIRA does not supported sorting in this case, so can proceed right away
            markFixedRowsAndColumns(element);
        } else {
            // if more than one fixed row need to disable sorting once tableenhancer is done
            $(table).on("tableenhancer-rownums-completed", function () {
                markFixedRowsAndColumns(element);
            });
        }
    });

    initializeBrowserDependentCorrections();


    /*
     * PART 4: Register the event handlers
     */

    $(winObj).scroll(scrollingHandler_window);
    $(".fixed-headers-container", docObj).scroll(scrollingHandler_LeftRight);

    $(winObj).on("load resize", refreshClones);

    // Provide an external trigger for the refresh function
    $(winObj).on("refreshClones.tableenhancer", refreshClones);

    if (environment !== "editor") {
        // handle mousedown on the cloned elements
        $(".fixed-headers-container", docObj).on("mousedown", "div.clone", cloneClickHandler);

        // handle sorting events
        $(".fixed-headers-container > table", docObj).on("sortEnd click", ".sortableHeader", refreshClones);
    }


    /*
     * PART 5: Function definitions
     */

    // Event handlers

    function scrollingHandler_window() {

        // Remove stickyTableHeaders, if they exist (must be done here, to always remove them, e.g. in case of reload, different loading order etc.)
        if (typeof $().stickyTableHeaders === "function") {
            $(".fixed-headers-container table").stickyTableHeaders("destroy");
        }

        // iterate over the tables within the macro on the page
        for (current_table_number = 0; current_table_number < number_of_tables_with_macro; current_table_number++) {
            scrollingHandler_UpDown();
            if (environment === "editor") {
                scrollingHandler_LeftRight();
            }
        }
    }

    function scrollingHandler_UpDown() {
        table_in_macro = $("div." + current_table_number, docObj).find("table:not(.clone):not(.confluenceTable .confluenceTable)");
        fixed_row_element = table_in_macro.find(".tableenhancer-fixed-row");
        if (fixed_row_element.length > 0) {
            updateScrollPosition("UpDown");
            if (isTableInView() && isFixedElementOutOfView_UpDown()) {
                if ($('.vert_clone.' + current_table_number, docObj).length === 0) {
                    createClone("UpDown");
                    configureVertCloneWrapperDiv();
                    createCornerCloneFromHoriClone();
                } else {
                    updateExistingClones("UpDown");
                }
            }
            else { // so table is not in view or the fixed row is, so delete row & corner clones
                removeClones("UpDown");
            }
        }
    }

    function scrollingHandler_LeftRight() {
        originating_div = (environment === "editor" ? $(".fixed-headers-container." + current_table_number, docObj) : $(this));
        table_in_macro = originating_div.find("table:not(.clone):not(.confluenceTable .confluenceTable)");
        fixed_column_element = table_in_macro.find(".tableenhancer-fixed-column");
        if (fixed_column_element.length > 0) {
            current_table_number = $.trim(originating_div.attr("class").match(/(\s[0-9]+$|^[0-9]+\s|\s[0-9]+\s|^[0-9]+$)/g));
            updateScrollPosition("LeftRight");
            if (isFixedElementOutOfView_LeftRight()) {
                if ($('.hori_clone.' + current_table_number, docObj).length === 0) {
                    createClone("LeftRight");
                    configureHoriCloneWrapperDiv();
                    createCornerCloneFromVertClone();
                } else {
                    updateExistingClones("LeftRight");
                }
                redrawIfNecessary();
            }
            else {
                removeClones("LeftRight");
            }
        }
    }

    function refreshClones() {
        calculateNumberAndHeightOfBottomRows();
        removeClones("All");
        scrollingHandler_window();
        if (environment !== "editor") {
            $(".fixed-headers-container", docObj).each(scrollingHandler_LeftRight);
        }
    }

    function cloneClickHandler(eventObj) {
        var target = $(eventObj.target).closest(".fixed-headers-container th, .fixed-headers-container td");
        if (target.hasClass('sortableHeader')) {
            var cell_index = $(target).index();
            var parent_wrapper_div = $(target).parents('.fixed-headers-container');
            var matching_element = $(parent_wrapper_div).children("table").find("thead > tr > *").eq(cell_index);
            $(matching_element[0]).mousedown(); // mousedown on the matching_element to kick off the sorting
            $(matching_element[0]).mouseup();
            $(matching_element[0]).click();
        }
    }


    // Helper functions

    function isTableInView() {
        return ((scroll_bottom > element_top) && (scroll_top < anchor_bottom));
    }

    function isFixedElementOutOfView_UpDown() {
        return (scroll_top > element_top);
    }

    function isFixedElementOutOfView_LeftRight() {
        if (environment === "editor") {
            return (element_left < scroll_left);
        } else {
            return ((0 > element_left));
        }
    }

    function createClone(scrollDirection) {
        var created_clone = table_in_macro
            .clone()
            .addClass('clone synchrony-exclude ')
            .appendTo($(".fixed-headers-container." + current_table_number, docObj));
        if (scrollDirection === "UpDown") {
            $(created_clone)
                .addClass('vert_clone ' + current_table_number)
                .css({
                    width: table_in_macro.outerWidth() + 'px',
                    top: table_top_correction + 'px'
                })
                .wrap('<div class="clone vert_clone synchrony-exclude ' + current_table_number + '"></div>');
        } else if (scrollDirection === "LeftRight") {
            $(created_clone)
                .addClass('hori_clone ' + current_table_number)
                .css({
                    height: table_in_macro.outerHeight() + 'px',
                    left: table_left_correction + 'px'
                })
                .wrap('<div class="clone hori_clone synchrony-exclude ' + current_table_number + '"></div>');
        } else {
            throw new Error("Unknown scroll direction!");
        }
    }

    function configureVertCloneWrapperDiv() {
        var wrapperDiv = $("div.vert_clone." + current_table_number, docObj)
            .css({
                height: (element_height + 1) + 'px',
                width: table_in_macro.outerWidth() - 1 + 'px'
            });
        configureOffsetTopBottomForDiv(wrapperDiv);
    }

    function configureHoriCloneWrapperDiv() {
        $("div.hori_clone." + current_table_number, docObj)
            .css({
                height: table_in_macro.outerHeight() + getSizeOfAutoCursorTargetIfExistent() + 'px',
                width: (element_width + 2) + 'px',
                top: getMarginTopOfConfluenceTableInEditor() + 'px',
                left: (environment === "editor" ? (scroll_left - element_left) : (scroll_left)) + div_left_correction + 'px'
            });
    }

    function createCornerCloneFromVertClone() {
        var vert_clone_div = $("div.vert_clone." + current_table_number, docObj);
        if (vert_clone_div.length > 0) { // if a vertical clone exists, create a corner clone
            updateScrollPosition("UpDown");
            var cornerClone = vert_clone_div.clone()
                .addClass('clone corner_clone synchrony-exclude ' + current_table_number)
                .css({
                    height: (element_height + 1) + 'px',
                    width: (element_width + 1) + 'px',
                    left: (environment === "editor" ? (scroll_left - element_left) : (scroll_left)) + div_left_correction + 'px'
                })
                .appendTo(vert_clone_div.parent());
            cornerClone.children('table')
                .removeClass('vert_clone hori_clone')
                .addClass('clone corner_clone synchrony-exclude ' + current_table_number)
                .css({
                    height: table_in_macro.outerHeight() + 'px',
                    left: table_left_correction + 'px'
                });
        }
    }

    function createCornerCloneFromHoriClone() {
        var hori_clone_div = $("div.hori_clone." + current_table_number, docObj);
        if (hori_clone_div.length > 0) { // if a horizontal clone exists, create a corner clone
            originating_div = hori_clone_div.parent();
            updateScrollPosition("LeftRight");
            var cornerClone = hori_clone_div.clone()
                .addClass('clone corner_clone synchrony-exclude ' + current_table_number)
                .css({
                    height: (element_height + 1) + 'px',
                    width: (element_width + 1) + 'px'
                })
                .appendTo(originating_div);
            configureOffsetTopBottomForDiv(cornerClone);
            cornerClone.children('table')
                .removeClass('vert_clone hori_clone')
                .addClass('clone corner_clone synchrony-exclude ' + current_table_number)
                .css({
                    width: table_in_macro.outerWidth() + 'px',
                    top: table_top_correction + 'px'
                });
        }
    }

    function configureOffsetTopBottomForDiv(passed_div) {
        // The Auto cursor gets added by collaborative editing
        var autocursor_p_size = getSizeOfAutoCursorTargetIfExistent() + getMarginTopOfConfluenceTableInEditor();
        if (scroll_top > anchor_bottom - element_height - height_of_bottom_x_rows[current_table_number]) {
            $(passed_div).css({
                top: '',
                bottom: height_of_bottom_x_rows[current_table_number] + 'px'
            });
        } else {
            $(passed_div).css({
                bottom: '',
                top: div_top_correction + ((scroll_top + autocursor_p_size - element_top > 0) ? (scroll_top + autocursor_p_size - element_top) : 0) + 'px'
            });
        }
    }

    function getSizeOfAutoCursorTargetIfExistent() {
        if (environment === "editor") {
            var autocursor_p_tags = $("#wysiwygTextarea_ifr").contents().find(".fixed-headers-container").children("p.auto-cursor-target");
            if (autocursor_p_tags.length > 0) {
                return autocursor_p_tags[0].offsetHeight;
            }
        }
        return 0;
    }

    function getMarginTopOfConfluenceTableInEditor() {
        if (environment === "editor") {
            return parseInt($(table_in_macro).css("margin-top"));
        }
        return 0;
    }

    function updateExistingClones(scrollDirection) {
        if (scrollDirection === "UpDown") {
            configureOffsetTopBottomForDiv($("div.vert_clone." + current_table_number, docObj));
            configureOffsetTopBottomForDiv($("div.corner_clone." + current_table_number, docObj));
        } else if (scrollDirection === "LeftRight") {
            $("div.hori_clone." + current_table_number, docObj).css({
                left: (environment === "editor" ? (scroll_left - element_left) : (scroll_left)) + div_left_correction + 'px'
            });
            $("div.corner_clone." + current_table_number, docObj).css({
                left: (environment === "editor" ? (scroll_left - element_left) : (scroll_left)) + div_left_correction + 'px'
            });
        } else {
            throw new Error("Unknown scroll direction!");
        }
    }

    function removeClones(scrollDirection) {
        if (scrollDirection === "UpDown") {
            $(".vert_clone." + current_table_number, docObj).remove();
        } else if (scrollDirection === "LeftRight") {
            $(".hori_clone." + current_table_number, docObj).remove();
        } else if (scrollDirection === "All") {
            $(".clone", docObj).remove();
        } else {
            throw new Error("unknown scroll direction!");
        }
        $(".corner_clone." + current_table_number, docObj).remove();
    }

    function updateScrollPosition(scrollDirection) {
        if (scrollDirection === "UpDown") {
            var header_correction = 0;
            if (environment === "display") {
                var mainHeaderAsOverlay = AJS.$("#main-header.overlay-header"),
                    mainHeaderCssTop = mainHeaderAsOverlay.css("top");
                if (mainHeaderCssTop === undefined || mainHeaderCssTop === "auto") {
                    header_correction = AJS.$("header#header.fixed-header").outerHeight();
                } else {
                    header_correction = mainHeaderAsOverlay.outerHeight() + parseInt(mainHeaderCssTop);
                }
            }
            scroll_top = $(winObj).scrollTop() + header_correction;
            element_top = table_in_macro.offset().top;
            fixed_row_element = table_in_macro.find(".tableenhancer-fixed-row");
            element_bottom = fixed_row_element.offset().top + fixed_row_element.outerHeight();
            element_height = Math.ceil(element_bottom - element_top);
            scroll_bottom = scroll_top + $(winObj).height();
            anchor_bottom = $("." + current_table_number + ".fixed-headers-bottom-anchor", docObj).offset().top;
        } else if (scrollDirection === "LeftRight") {
            fixed_column_element = table_in_macro.find(".tableenhancer-fixed-column");
            if (environment === "editor") {
                scroll_left = $(winObj).scrollLeft();
                element_left = table_in_macro.offset().left;
                element_right = fixed_column_element.offset().left + fixed_column_element.outerWidth();
            } else {
                scroll_left = originating_div.scrollLeft();
                element_left = table_in_macro.position().left;
                element_right = fixed_column_element.position().left + fixed_column_element.outerWidth();
            }
            element_width = Math.ceil(element_right - element_left);
        } else {
            throw new Error("unknown scroll direction!");
        }
    }

    function redrawIfNecessary() {
        if (browser === "Chrome") { // Chrome sometimes fails to redraw properly when scrolling horizontally
            $(anchors_for_refresh).hide().show(1);
        }
    }

    function markFixedRowsAndColumns(containerElement) {
        // - <tr> marked as class "tableenhancer-fixed-row"
        // - <td> or <th> in second-last row marked as class "tableenhancer-fixed-column", to avoid merged cells e.g. in header
        var fixed_rows = $(containerElement).attr("data-fixed-rows");
        var fixed_columns = $(containerElement).attr("data-fixed-columns");

        if (fixed_rows > 0) {
            $(containerElement).find("table > :not(tfoot) > tr:eq(" + (fixed_rows - 1) + ")").addClass("tableenhancer-fixed-row");
        }
        if (fixed_columns > 0) {
            $(containerElement).find("table tr").eq(-2).children(":nth-child(" + fixed_columns + ")").addClass("tableenhancer-fixed-column");
        }
        if (fixed_rows > 1) {
            if (environment !== "editor") {
                disableSorting(containerElement);
            }
        }

        calculateNumberAndHeightOfBottomRows();
    }

    function calculateNumberAndHeightOfBottomRows() {
        for (current_table_number = 0; current_table_number < number_of_tables_with_macro; current_table_number++) {
            table_in_macro = $("div." + current_table_number, docObj).find("table:not(.clone):not(.confluenceTable .confluenceTable)");
            fixed_row_element = table_in_macro.find(".tableenhancer-fixed-row");
            if (fixed_row_element.length > 0) {
                updateScrollPosition("UpDown");
                var covered_part_of_screen = COVER_BEFORE_SCROLL_OUT * $(winObj).height();
                var curr_height_of_bottom_x_rows = 0;
                var current_element_height;
                for (var x = 1; x < MAX_NUMBER_OF_KEPT_ROWS + 1; x++) {
                    current_element_height = table_in_macro.find("tr").eq(-x).height();
                    curr_height_of_bottom_x_rows = curr_height_of_bottom_x_rows + current_element_height;
                    if (curr_height_of_bottom_x_rows > covered_part_of_screen - element_height) {
                        break;
                    }
                }
                height_of_bottom_x_rows[current_table_number] = curr_height_of_bottom_x_rows - current_element_height;
                number_of_bottom_rows_kept[current_table_number] = x - 1;
            } else {
                height_of_bottom_x_rows[current_table_number] = 0;
                number_of_bottom_rows_kept[current_table_number] = 0;
            }
        }
    }

    function initializeBrowserDependentCorrections() {
        // determine the browser and adjust accordingly
        browser = getBrowser();
        if (browser === "Firefox") {
            div_top_correction = -1;
            table_top_correction = 1;
            div_left_correction = -1;
            table_left_correction = 1;
        } else {
            div_top_correction = 0;
            table_top_correction = 0;
            div_left_correction = 0;
            table_left_correction = 0;
        }
    }


    // Sorting related functions

    function disableSorting(container_element) {
        $(container_element).find("table").trigger("destroy");
        setTimeout(function () { // remove the CSS classes for the sorting (e.g. to remove hover effect)
            removeSortingFromTablesInside(container_element);
        }, 500);
    }

    function removeSortingFromTablesInside(container) {
        $(container).find(".tablesorter").removeClass("tablesorter");
        $(container).find(".sortableHeader").removeClass("sortableHeader");
        $(container).find(".tablesorter-header-inner").removeClass("tablesorter-header-inner");
    }


    // Table-unrelated helper functions

    function getBrowser() {
        var ua = navigator.userAgent, tem, M;
        M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
        if (/trident/i.test(M[1])) {
            tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
            return 'IE';
        }
        if (M[1] === 'Chrome') {
            tem = ua.match(/\bOPR\/(\d+)/);
            if (tem !== null) {
                return 'Opera';
            }
        }
        M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
        if ((tem = ua.match(/version\/(\d+)/i)) !== null) { M.splice(1, 1, tem[1]); }
        return M[0];
    }
};

AJS.$(window).on("activateFixedHeaders.tableenhancer", function(event, environment) {
    try {
        activateFixedHeaders(event, environment);
    } catch (e) {
        AJS.logError("table enhancer: error activating fixed headers", e);
    }
});

AJS.$(window).trigger("activateFixedHeaders.tableenhancer", ["display"]);
}); // AJS.toInit