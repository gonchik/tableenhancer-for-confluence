com.tngtech.confluence.plugins.tableenhancer.warning.sortColumnIndexNotParseable=Cannot parse {0} as an unsigned integer for 'sortColumn'.
com.tngtech.confluence.plugins.tableenhancer.warning.noTableFound=No table found as top level element in macro body.
com.tngtech.confluence.plugins.tableenhancer.warning.firstRowMustBeHeadingToSortTable=The first table row is no heading row, therefore this table is not sortable.
com.tngtech.confluence.plugins.tableenhancer.warning.sortColumnIndexOutOfBound=Please provide an index for 'sortColumn' between 1 and {0} (= column count before row numbering). {1} (= input) is not a valid column index.
com.tngtech.confluence.plugins.tableenhancer.warning.numberOfFixedRowsNotParseable=Cannot parse {0} as an integer for 'numberOfFixedRows'.
com.tngtech.confluence.plugins.tableenhancer.warning.numberOfFixedColumnsNotParseable=Cannot parse {0} as an integer for 'numberOfFixedColumns'.

com.tngtech.confluence.plugins.tableenhancer.tableenhancer.label=Tableenhancer
com.tngtech.confluence.plugins.tableenhancer.tableenhancer.desc=The Tableenhancer macro provides enhancements for a normal table such as initial sorting by a supplied column or automatic row numbering. You can find a more detailed documetation of parameters <a href="https://tngtech-oss.atlassian.net/wiki/display/TABENH/Tableenhancer+for+Confluence+Home#TableenhancerforConfluenceHome-Parameters">here</a>.

com.tngtech.confluence.plugins.tableenhancer.tableenhancer.param.rowNumbers.label=Row numbering
com.tngtech.confluence.plugins.tableenhancer.tableenhancer.param.rowNumbers.desc=Adds a consecutive number starting with 1 as first cell to every row. Options: <ul><li><i>none</i>: <b>no</b> numbering</li><li><i>onceBeforeSorting</i>: once <b>before</b> initial sorting</li><li><i>independentFromSorting</i>: completely <b>independent</b> from every sorting</li><li><i>onceAfterSorting</i>: once <b>after</b> initial sorting</li></ul>

com.tngtech.confluence.plugins.tableenhancer.tableenhancer.param.rowNumbersBgColor.label=Background color for row numbers
com.tngtech.confluence.plugins.tableenhancer.tableenhancer.param.rowNumbersBgColor.desc=Choose the background color for the row number cells. Option <i>asRightNextCell</i> clones color/formatting from the right next cell compared to the inserted row number cell.

com.tngtech.confluence.plugins.tableenhancer.tableenhancer.param.sortColumn.label=Sort column index
com.tngtech.confluence.plugins.tableenhancer.tableenhancer.param.sortColumn.desc=Index of the column after which should be sorted (starting by 1, not counting row number column).

com.tngtech.confluence.plugins.tableenhancer.tableenhancer.param.sortDescending.label=Sort descending
com.tngtech.confluence.plugins.tableenhancer.tableenhancer.param.sortDescending.desc=Choose this option if sort order of chosen column should be descending.

com.tngtech.confluence.plugins.tableenhancer.tableenhancer.param.totalLine.label=Total line
com.tngtech.confluence.plugins.tableenhancer.tableenhancer.param.totalLine.desc=Whether to show a total line at the end of the table containing sums for all columns with only summable numbers.

com.tngtech.confluence.plugins.tableenhancer.tableenhancer.param.decimalMark.label=Decimal mark
com.tngtech.confluence.plugins.tableenhancer.tableenhancer.param.decimalMark.desc=Choose either <i>. (point)</i> or <i>, (comma)</i> as decimal separator for parsing and summing numeric values for total line.

com.tngtech.confluence.plugins.tableenhancer.tableenhancer.param.numberOfFixedRows.label=Number of rows to keep visible
com.tngtech.confluence.plugins.tableenhancer.tableenhancer.param.numberOfFixedRows.desc=Choose the number of rows at the top of the table to be fixed while scrolling. Only available for the default theme.

com.tngtech.confluence.plugins.tableenhancer.tableenhancer.param.numberOfFixedColumns.label=Number of columns to keep visible
com.tngtech.confluence.plugins.tableenhancer.tableenhancer.param.numberOfFixedColumns.desc=Choose the number of columns on the left of the table to be fixed while scrolling (including a possible row numbering column). Only available for the default theme.

com.tngtech.confluence.plugins.tableenhancer.tableenhancer.body.label=Macrobody containing table to be enhanced
com.tngtech.confluence.plugins.tableenhancer.tableenhancer.body.desc=The macro body with the table which should be enhanced. This table has to a top level element within the macro body, therefore must not be within other elements.
