Tableenhancer for Confluence
============================

What is it
----------

The tableenhancer plugin provides enhancements for a normal Confluence table such as initial sorting by a
supplied column or automatic row numbering.

More information can be found on 
[Atlassian Marketplace - Tableenhancer](https://marketplace.atlassian.com/plugins/com.tngtech.confluence.plugins.tableenhancer).

Eclipse
-------

Hint: For generating Eclipse project run ```gradle eclipse``` and for IntelliJ IDEA import as a Gradle project which will
handle Groovy nature of this project much better.
